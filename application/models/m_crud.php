<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_crud extends CI_Model {

	public function get_all($table, $limit, $offset)
	{
		return $this->db->get($table, $limit, $offset);
	}

	public function no_paging($table)
	{
		return $this->db->get($table);
	}

	public function get_id($table, $pk, $id)
	{
		$this->db->where($pk, $id);
		return $this->db->get($table);
	}

	public function get_kategori($table, $cat, $status)
	{
		$this->db->where($cat, $status);
		return $this->db->get($table);
	}


	public function count($table)
	{
		return $this->db->count_all($table);
	}

	public function countfilter($table, $pk, $cat)
	{
		$this->db->where($pk, $cat);
		return $this->db->count_all_results($table, $pk, $cat);
	}

	public function insertData($table, $record)
	{
		$this->db->insert($table, $record);
	}

	public function updateData($table, $record, $pk, $id)
	{
		$this->db->where($pk, $id);
		$this->db->update($table, $record);
	}

	public function deleteData($table, $pk, $id)
	{
		$this->db->where($pk, $id);
		$this->db->delete($table);
	}

	public function delete($table, $pk, $id)
    {
		$this->db->where($pk, $id);
		return $this->db->delete($table);
    }

	public function login($table, $email, $password, $status)
	{
		$this->db->where('email', $email);
		$this->db->where('password', $password);
		$this->db->where('status', $status);
		return $this->db->get($table);
	}

	public function loginmember($table, $email, $password, $status)
	{
		$this->db->where('id_anggota', $email);
		$this->db->or_where('email', $email);
		$this->db->where('password', $password);
		$this->db->where('status', $status);
		return $this->db->get($table);
	}

	public function idanggota($table, $email)
	{
		$this->db->where('email', $email);
		return $this->db->get($table);
	}

	public function cekPassword($table, $email)
	{
		$this->db->where('email', $email);
		return $this->db->get($table);
	}
	// Check username exists
public function check_username_exists($username){
 $query = $this->db->get_where('t_anggota', array('username' => $username));
 if(empty($query->row_array())){
   return true;
     } else {
  return false;
 }
}

// Check phone number exists
public function check_phone_exists($phone){
$query = $this->db->get_where('t_anggota', array('hp' => $phone));
if(empty($query->row_array())){
 return true;
	 } else {
return false;
}
}

// Check phone number exists
public function check_email_exists($email){
$query = $this->db->get_where('t_anggota', array('email' => $email));
if(empty($query->row_array())){
 return true;
	 } else {
return false;
}
}

// Check phone number exists
public function check_email_user_exists($email){
	$query = $this->db->get_where('t_user', array('email' => $email));
	if(empty($query->row_array())){
	 return true;
		 } else {
	return false;
	}
	}

// Check email does exists
public function check_email_not_exists($email){
$query = $this->db->get_where('t_anggota', array('email' => $email));
if(empty($query->row_array())){
 return false;
	 } else {
return true;
}
}

	// Check email does exists
public function check_kategori_exists($kategori){
	$query = $this->db->get_where('t_kategori', array('kategori' => $kategori));
	if(empty($query->row_array())){
	 return true;
		 } else {
	return false;
	}
	}

	public function get_temp()
	{
		return $this->db->get('temp')->result();
	}

	public function hapus_temp()
	{
		$this->db->empty_table('temp');
	}

	public function autoNumber($table, $kolom, $lebar=0, $awalan=null)
	{
		$this->db->select($kolom);
		$this->db->limit(1);
		$this->db->order_by($kolom, 'desc');
		$this->db->from($table);
		$query = $this->db->get();

		$row = $query->result_array();
		$cek = $query->num_rows();

		if ($cek == 0)
			$nomor = 1;
		else
		{
			$nomor = intval(substr($row[0][$kolom], strlen($awalan)))+1;
		}

			if ($lebar > 0)
			{
				$result = $awalan.str_pad($nomor, $lebar, "0", STR_PAD_LEFT);
			}
			else
			{
				$result = $awalan.$nomor;
			}

			return $result;
	}

	public function kategori()
	{
		$datacat = $this->db->get('t_kategori')->result();
		return $datacat;

	}

	// cari data peminjam untuk pengembalian buku
	public function data_peminjam($id)
	{
		$this->db->select('t_peminjaman.*, t_anggota.nama, t_buku.judul, t_buku.pengarang');
		$this->db->from('t_peminjaman');
		$this->db->join('t_anggota', 't_anggota.id_anggota = t_peminjaman.id_anggota');
		$this->db->join('t_buku', 't_buku.kd_buku = t_peminjaman.kd_buku');
		$this->db->where('t_peminjaman.status', 'n');
		$this->db->where('t_peminjaman.id_anggota', $id);
		return $this->db->get();
	}

		// cari data peminjam untuk pengembalian buku
		public function requestakses()
		{
			$this->db->select('t_buku_berbayar.*, t_anggota.nama, t_buku.judul');
			$this->db->from('t_buku_berbayar');
			$this->db->join('t_anggota', 't_anggota.id_anggota = t_buku_berbayar.id_anggota');
			$this->db->join('t_buku', 't_buku.kd_buku = t_buku_berbayar.kd_buku');
			//$this->db->where('t_buku.berbayar', 'Y');
			//$this->db->where('t_peminjaman.id_anggota', $id);
			return $this->db->get();
		}

		public function get_request_id($pk, $aid)
		{
			$this->db->select('t_buku_berbayar.*, t_anggota.nama, t_buku.judul');
			$this->db->from('t_buku_berbayar');
			$this->db->join('t_anggota', 't_anggota.id_anggota = t_buku_berbayar.id_anggota');
			$this->db->join('t_buku', 't_buku.kd_buku = t_buku_berbayar.kd_buku');
			//$this->db->where('t_buku.berbayar', 'Y');
			$this->db->where('t_buku_berbayar.id', $aid);
			//$this->db->where($aid);
			return $this->db->get();
		}

		public function bukusaya()
		{
			$this->db->select('t_statistik_buku.*, t_anggota.nama, t_buku.judul, t_buku.penerbit, t_buku.pengarang, t_buku.berbayar');
			$this->db->from('t_statistik_buku');
			$this->db->join('t_anggota', 't_anggota.id_anggota = t_statistik_buku.id_anggota');
			$this->db->join('t_buku', 't_buku.kd_buku = t_statistik_buku.kd_buku');
			//$this->db->where('t_buku.berbayar', 'Y');
			//$this->db->where('t_anggota.email', $aid);
			//$this->db->where($aid);
			return $this->db->get();
		}

		public function listbuku()
		{
			$this->db->select('t_buku.*, t_kategori.*');
			$this->db->from('t_buku');
			$this->db->join('t_kategori', 't_kategori.kode = t_buku.kategori');
			//$this->db->where('t_buku.berbayar', 'Y');
			//$this->db->where($aid);
			return $this->db->get();
		}



}

/* End of file m_crud.php */
/* Location: ./application/models/m_crud.php */

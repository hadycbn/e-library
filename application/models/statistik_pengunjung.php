<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statistik_pengunjung extends CI_Model {
	const TABLE_PENGUNJUNG = 't_statistik_pengunjung';

	// statistik pengunjung online
	public function count_pengunjung_online()
	{
		$dateNow = date('Y-m-d');
		$this->db->from(self::TABLE_PENGUNJUNG);
		$this->db->where('tanggal', $dateNow);
		return $this->db->count_all_results();
	}

	// statistik pengunjung hari ini
	public function count_pengunjung_hari_ini()
	{
		$dateNow = date('Y-m-d');
		$this->db->from(self::TABLE_PENGUNJUNG);
		$this->db->where('tanggal', $dateNow);
		return $this->db->count_all_results();
	}

	// statistik pengunjung bulan ini
	public function count_pengunjung_bulan_ini()
	{
		$this->db->from(self::TABLE_PENGUNJUNG);
		$this->db->where('MONTH(tanggal) = MONTH(CURRENT_DATE()) AND YEAR(tanggal) = YEAR(CURRENT_DATE())');
		return $this->db->count_all_results();
	}

	// statistik total pengunjung
	public function count_total_pengunjung()
	{
		return $this->db->count_all(self::TABLE_PENGUNJUNG);
	}

	// statistik grafik pengunjung satu tahun terakhir
	public function grafik_pengunjung_tahun_ini()
	{
		$data = [];
		for ($i = 12 - 1; $i >= 0; $i--) {
			$timestamp = mktime(0, 0, 0, date('n') - $i, 1);

		    $this->db->select('COUNT(*) AS jumlah_pengunjung');
			$this->db->from(self::TABLE_PENGUNJUNG);
			$this->db->where('MONTHNAME(tanggal) = "'. date('F', $timestamp) .'" AND YEAR(tanggal) = "'. date('Y', $timestamp) .'"');
			$this->db->limit(1);

			$result = $this->db->get()->row('jumlah_pengunjung');
			$data [date('F Y', $timestamp)]= $result;
		}
		return $data;
	}
}
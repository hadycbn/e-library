<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statistik_anggota extends CI_Model {
	const TABLE_ANGGOTA = 't_anggota';

	// statistik anggota hari ini
	public function count_anggota_hari_ini()
	{
		$dateNow = date('Y-m-d');
		$this->db->from(self::TABLE_ANGGOTA);
		$this->db->where('tgl_daftar', $dateNow);
		return $this->db->count_all_results();
	}

	// statistik anggota minggu ini
	public function count_anggota_minggu_ini()
	{
		$dateNow = date('Y-m-d', strtotime('-1 weeks'));
		$this->db->from(self::TABLE_ANGGOTA);
		$this->db->where('tgl_daftar >=', $dateNow);
		return $this->db->count_all_results();
	}

	// statistik anggota bulan ini
	public function count_anggota_bulan_ini()
	{
		$this->db->from(self::TABLE_ANGGOTA);
		$this->db->where('MONTH(tgl_daftar) = MONTH(CURRENT_DATE()) AND YEAR(tgl_daftar) = YEAR(CURRENT_DATE())');
		return $this->db->count_all_results();
	}

	// statistik total anggota
	public function count_total_anggota()
	{
		return $this->db->count_all(self::TABLE_ANGGOTA);
	}

	// statistik grafik anggota satu tahun terakhir
	public function grafik_anggota_tahun_ini()
	{
		$data = [];
		for ($i = 12 - 1; $i >= 0; $i--) {
			$timestamp = mktime(0, 0, 0, date('n') - $i, 1);

		    $this->db->select('COUNT(*) AS jumlah_anggota');
			$this->db->from(self::TABLE_ANGGOTA);
			$this->db->where('MONTHNAME(tgl_daftar) = "'. date('F', $timestamp) .'" AND YEAR(tgl_daftar) = "'. date('Y', $timestamp) .'"');
			$this->db->limit(1);

			$result = $this->db->get()->row('jumlah_anggota');
			$data [date('F Y', $timestamp)]= $result;
		}
		return $data;
	}
}
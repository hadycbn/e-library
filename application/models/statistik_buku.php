<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statistik_buku extends CI_Model {
	const TABLE_BUKU = 't_buku';
	const TABLE_STATISTIK_BUKU = 't_statistik_buku';

	// statistik buku gratis
	public function count_buku_gratis()
	{
		$this->db->from(self::TABLE_BUKU);
		$this->db->where('berbayar', 'N');
		return $this->db->count_all_results();
	}

	// statistik buku berbayar
	public function count_buku_berbayar()
	{
		$this->db->from(self::TABLE_BUKU);
		$this->db->where('berbayar', 'Y');
		return $this->db->count_all_results();
	}

	// statistik buku dibaca
	public function count_buku_dibaca()
	{
		$this->db->select(self::TABLE_BUKU .'.kd_buku');
		$this->db->from(self::TABLE_BUKU);
		$this->db->join(self::TABLE_STATISTIK_BUKU, self::TABLE_STATISTIK_BUKU . '.kd_buku = ' . self::TABLE_BUKU . '.kd_buku');
		$this->db->where(self::TABLE_STATISTIK_BUKU . '.keterangan', 'BACA');
		$this->db->group_by(self::TABLE_BUKU . '.kd_buku');
		return $this->db->get()->num_rows();
	}

	// statistik buku didownload
	public function count_buku_didownload()
	{
		$this->db->select(self::TABLE_BUKU .'.kd_buku');
		$this->db->from(self::TABLE_BUKU);
		$this->db->join(self::TABLE_STATISTIK_BUKU, self::TABLE_STATISTIK_BUKU . '.kd_buku = ' . self::TABLE_BUKU . '.kd_buku');
		$this->db->where(self::TABLE_STATISTIK_BUKU . '.keterangan', 'DOWNLOAD');
		$this->db->group_by(self::TABLE_BUKU . '.kd_buku');
		return $this->db->get()->num_rows();
	}

	// statistik total buku
	public function count_total_buku()
	{
		return $this->db->count_all(self::TABLE_BUKU);
	}

	// statistik top 5 buku
	public function top_buku_paling_banyak_dibaca(int $limit = 5)
	{
		$this->db->select(self::TABLE_BUKU .'.kd_buku, '. self::TABLE_BUKU .'.judul, COUNT('. self::TABLE_STATISTIK_BUKU .'.kd_buku) AS jumlah_dibaca', false);
		$this->db->from(self::TABLE_BUKU);
		$this->db->join(self::TABLE_STATISTIK_BUKU, self::TABLE_STATISTIK_BUKU . '.kd_buku = ' . self::TABLE_BUKU . '.kd_buku');
		$this->db->where(self::TABLE_STATISTIK_BUKU . '.keterangan', 'BACA');
		$this->db->group_by([self::TABLE_BUKU . '.kd_buku', self::TABLE_BUKU .'.judul']);
		$this->db->order_by('COUNT('. self::TABLE_STATISTIK_BUKU .'.kd_buku)', 'DESC');
		$this->db->limit($limit);
		$result = $this->db->get();
		return $result->num_rows() > 0 ? $result->result_array() : [];
	}
}
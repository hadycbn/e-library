<div id="buku">

	<div class="panel panel-info">
		<div class="panel-body">

			<div class="table-responsive">
				<div class="panel panel-warning">
					<div class="panel-body">
						<form action="<?php echo site_url('petugas/cariData') ?>" method="post">
						<div class="col-sm-4 pull-right">
							<div class="form-group pull-right">
							  <div class="input-group">

							  </div>
							</div>
						</div>
						</form>
						<table class="table table-striped">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama Kategori</th>
									<th>Status Kategori</th>
								</tr>
							</thead>
							<tbody>
								<?php $no=1; foreach ($kategori as $k): ?>
									<tr>
										<td width="50"><?php echo $no++; ?></td>
										<td width="200">
											<?php echo $k->kategori ?><br>
											<!--<a href="<?php //echo site_url('kategori/edit/'.$k->kode) ?>">Edit</a>&nbsp;|&nbsp;-->
											<a href="#" class="hapus" kode="<?php echo $k->kode ?>">Hapus Kategori</a>&nbsp;&nbsp;
										</td>
										<td width="50"><?php
										if ($k->status==1) { echo "Aktif";
										}else {echo "Tidak Aktif";}
										 ?></td>

									</tr>
								<?php endforeach ?>
							</tbody>
						</table>

					</div>
				</div>
			</div>
			<div class="well well-sm">
				<a href="<?php echo site_url('kategori') ?>" class="btn btn-danger">Batal / Kembali</a>
			</div>
		</div>
	</div>
</div>

<script>
	$(function(){

		//delay alert
		$('#delay-alert').delay(2000).hide(100);

		//delete buku
		$('.hapus').click(function(){
			var kode = $(this).attr('kode');
			$('#idhapus').val(kode);
			$('#modal-delete').modal('show');
		});

		$('#konfirmasi').click(function(){
			var kode = $("#idhapus").val();

			$.ajax({
				url  : "<?php echo site_url('buku/hapus') ?>",
				type : "POST",
				data : "id_hapus="+kode,
				success : function(html){
					location.reload();
				}
			});
		});
	})
</script>

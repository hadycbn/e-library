<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("_partials/head.php") ?>
</head>
<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
      <?php $this->load->view("_partials/sidebar.php") ?>


        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">
              <?php $this->load->view("_partials/navbar.php") ?>


							<!-- Begin Page Content -->
							                <div class="container-fluid">

							                    <!-- Page Heading -->
							                    <h1 class="h3 mb-2 text-gray-800">Daftar Kategori Buku</h1>
							                    <p class="mb-4"><!--DataTables is a third party plugin that is used to generate the demo table below.
							                        For more information about DataTables, please visit the <a target="_blank"
							                            href="https://datatables.net">official DataTables documentation</a>.--></p>

							                    <!-- DataTales Example -->
                                                <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
				<?php foreach ($kategori as $k): ?>
                    <a href="<?php echo site_url('buku/kategori/'.$k->kode) ?>"><div class="col-lg-3 mb-4">
    <div class="<?php echo $k->template ?>">
        <div class="card-body" align="center"><font color="#FFF"><?php echo $k->kategori ?></font><div>
        </div>
            </div></a>
        </div>
    </div>
	<?php endforeach ?>
    <!--<div class="col-lg-3 mb-4">
        <div class="card bg-success text-white shadow">
            <div class="card-body">
                ILMU PENGETAHUAN
                <div></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 mb-4">
        <div class="card bg-info text-white shadow">
            <div class="card-body">
                CERITA BERGAMBAR
                  <div></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 mb-4">
        <div class="card bg-warning text-white shadow">
            <div class="card-body">
                Warning
                <div></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 mb-4">
        <div class="card bg-danger text-white shadow">
            <div class="card-body">
                Danger
                <div></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 mb-4">
        <div class="card bg-secondary text-white shadow">
            <div class="card-body">
                Secondary
                <div></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 mb-4">
        <div class="card bg-light text-black shadow">
            <div class="card-body">
                Light
                <div></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 mb-4">
        <div class="card bg-dark text-white shadow">
            <div class="card-body">
                Dark
                <div></div>
            </div>
        </div>
    </div>
</div>-->

</div>
            </div>
        </div>
							                <!-- /.container-fluid -->

							            </div>
							            <!-- End of Main Content -->

                <?php $this->load->view("_partials/footer.php") ?>
            </div>
            <!-- End of Main Content -->



        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <?php $this->load->view("_partials/scrolltop.php") ?>
    <?php $this->load->view("_partials/modal.php") ?>
    <?php $this->load->view("_partials/js.php") ?>




</body>

</html>

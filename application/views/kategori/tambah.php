<div id="tambahbuku">
	<div class="panel panel-info">
		<div class="panel-heading">
			Form Tambah Kategori
		</div>
		<div class="panel-body">
			<form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
				<div class="form-group">
					<label for="" class="col-md-3 control-label">Nama Kategori:</label>
					<div class="col-md-5">
						<input type="text" name="nama" class="form-control" value="<?php echo $this->input->post('nama') ?>">
						<?php echo form_error('nama') ?>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-md-3 control-label">Status Kategori :</label>
					<div class="col-md-3">
						<select name="status" class="form-control">
							<option value="">--Pilih Status--</option>
							<option value="1">Aktif</option>
							<option value="2">Tidak Aktif</option>
						</select>
						<?php echo form_error('status') ?>
					</div>
				</div>
		</div>
		<div class="panel-footer">
			<div class="container-fluid">
				<button class="btn btn-primary">Simpan Data</button>
				<a href="<?php echo site_url('kategori') ?>" class="btn btn-danger">Batal / Kembali</a>
			</div>
		</div>
		</form>
	</div>
</div>

<?php foreach ($anggota as $a): ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("_partials/head.php") ?>
</head>
<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
      <?php $this->load->view("_partials/sidebar.php") ?>


        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
              <?php $this->load->view("_partials/navbar.php") ?>


							<!-- Begin Page Content -->
                            
							                <div class="container-fluid">

							                    <!-- Page Heading -->
							                    <h1 class="h3 mb-2 text-gray-800">Profil Saya</h1>
							                    <p class="mb-4"><!--DataTables is a third party plugin that is used to generate the demo table below.
							                        For more information about DataTables, please visit the <a target="_blank"
							                            href="https://datatables.net">official DataTables documentation</a>.--></p>

							                    <!-- DataTales Example -->
                                                <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div></div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
							<div class="alert-delay">
		<?php 
			echo $this->session->flashdata('success'); 
		?>
	</div>
                            </div>
                           
                            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
									<?php
												if($a->foto == "")
												{
													echo '<img src="'.base_url('assets/img/no_photo.png').'" alt="" width="150" height="170">';
												}
												else{
													echo '<img src="'.base_url('assets/img/anggota/'.$a->foto).'" alt="" width="150" height="170">';
												}
											?> 
                                    </div>
                                    <div class="col-sm-6">
                                    </div>
                                </div>
								<div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                       ID Anggota
                                    </div>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control form-control-user" name="id_anggota" value="<?php echo $a->id_anggota ?> " readonly>
                                    </div>
                                </div>    
								<div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    Username
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-user" name="phone" value="<?php echo $a->username ?>" readonly>
                                            <?php echo form_error('phone') ?>
                                    </div>
                                </div>                        
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                    Nama Anggota
                                    </div>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control form-control-user" name="nama" value="<?php echo $a->nama ?>">
                                        <?php echo form_error('nama') ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                    Email
                                    </div>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control form-control-user" name="email" value="<?php echo $a->email ?>">
                                        <?php echo form_error('email') ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    No Handphone
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-user" name="phone" value="<?php echo $a->hp ?>">
                                            <?php echo form_error('phone') ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                   Foto Anggota
                                    </div>
                                    <div class="col-sm-6">
                                    <input type="file" name="foto" class="form-control" accept="image/*" />
                                   
                                    </div>
                                </div>
                                <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    Status Anggota
                                    </div>
                                    <div class="col-sm-6">
                                    <select name="status" class="form-control" readonly>
							<?php  
								$array = array('Y' =>'AKTIF', 'N' => 'TIDAK AKTIF');
								foreach ($array as $value => $text) {
									$selected = ($value == $a->status)?"selected":"";
									echo '<option value="'.$value.'" '.$selected.'>'.ucwords($text).'</option>';
								}
							?>
						</select>
						<?php echo form_error('level') ?>
                                    </div>
                                </div>
								<div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                   Tanggal Daftar
                                    </div>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control form-control-user" name="tgl" value="<?php echo $a->tgl_daftar ?>" readonly>
                                   
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                         
                                    </div>
                                </div>
                               <button class="btn btn-primary">Simpan Perubahan</button>&nbsp;
                               <a href="<?php echo site_url('dashboard')?>" class="btn btn-primary">
                                    Kembali
                                </a>
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
							                <!-- /.container-fluid -->

							            </div>
                                        
							            <!-- End of Main Content -->
                                                       
                <?php $this->load->view("_partials/footer.php") ?>
            </div>
            <!-- End of Main Content -->



        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <?php $this->load->view("_partials/scrolltop.php") ?>
    <?php $this->load->view("_partials/modal.php") ?>
    <?php $this->load->view("_partials/js.php") ?>




</body>

</html>
<?php endforeach ?>
<script>
	$(function(){
		$('.alert-delay').delay(7000).fadeOut(7000);
	});
</script>
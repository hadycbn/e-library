<?php foreach ($anggota as $a): ?>
<div id="row">
	<div class="col-md-10">
		<div class="panel panel-info">
			<div class="panel-heading">
				Form Edit Anggota
			</div>
			<div class="panel-body">
			<form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                       ID Anggota
                                    </div>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control form-control-user" name="id_anggota" value="<?php echo $a->id_anggota ?>" disabled>
                                    </div>
                                </div>                            
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                    Nama Anggota
                                    </div>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control form-control-user" name="nama" value="<?php echo $a->nama ?>">
                                        <?php echo form_error('nama') ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                    Email
                                    </div>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control form-control-user" name="editemail" value="<?php echo $a->email ?>">
                                        <?php echo form_error('editphone') ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    No Handphone
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-user" name="editphone" value="<?php echo $a->hp ?>">
                                            <?php echo form_error('editphone') ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                   Foto Anggota
                                    </div>
                                    <div class="col-sm-6">
                                    <input type="file" name="foto" class="form-control">
                                    <?php echo form_error('foto') ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    Status Anggota
                                    </div>
                                    <div class="col-sm-6">
                                    <select name="status" class="form-control">
							<?php  
								$array = array('Y' =>'AKTIF', 'N' => 'TIDAK AKTIF');
								foreach ($array as $value => $text) {
									$selected = ($value == $a->status)?"selected":"";
									echo '<option value="'.$value.'" '.$selected.'>'.ucwords($text).'</option>';
								}
							?>
						</select>
						
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                         
                                    </div>
                                </div>
                               <button class="btn btn-primary">Simpan Perubahan</button>&nbsp;
                               <a href="<?php echo site_url('admin/anggota')?>" class="btn btn-primary">
                                    Kembali
                                </a>
                            </form>
		</div>		
	</div>

	<div class="col-md-2">
		<img src="<?php echo base_url('assets/img/anggota/'.$a->foto) ?>" class="img-rounded" alt="" width="300" height="370">
	</div>
</div>	
<?php endforeach ?>

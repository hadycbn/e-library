<div id="tambahbuku">
	<div class="panel panel-info">
		<div class="panel-heading">
			Form Tambah Anggota
		</div>
		<div class="panel-body">
			<form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
				<div class="form-group">
					<label for="" class="col-md-3 control-label">Id Anggota :</label>
					<div class="col-md-3">
						<input type="text" name="id_anggota" class="form-control" value="<?php echo $autonumber ?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-md-3 control-label">Nama :</label>
					<div class="col-md-4">
						<input type="text" name="nama" class="form-control" value="<?php echo $this->input->post('nama') ?>">
						<?php echo form_error('nama') ?>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-md-3 control-label">Email :</label>
					<div class="col-md-4">
						<input type="text" name="email" class="form-control" value="<?php echo $this->input->post('email') ?>">
						<?php echo form_error('email') ?>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-md-3 control-label">No Handphone :</label>
					<div class="col-md-4">
						<input type="text" name="phone" class="form-control" value="<?php echo $this->input->post('phone') ?>">
						<?php echo form_error('phone') ?>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-md-3 control-label">Username :</label>
					<div class="col-md-4">
						<input type="text" name="username" class="form-control" value="<?php echo $this->input->post('username') ?>">
						<?php echo form_error('username') ?>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-md-3 control-label">Password :</label>
					<div class="col-md-4">
						<input type="password" name="password" class="form-control" value="<?php echo $this->input->post('password') ?>">
						<?php echo form_error('password') ?>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-md-3 control-label">Foto :</label>
					<div class="col-md-4">
						<input type="file" name="foto" class="form-control">
					</div>
				</div>
		</div>
		<div class="panel-footer">
			<div class="container-fluid">
				<button class="btn btn-primary">Simpan Data</button>
				<a href="<?php echo site_url('anggota') ?>" class="btn btn-danger">Batal / Kembali</a>
			</div>
		</div>
		</form>
	</div>
</div>

<div id="buku">

	<div class="panel panel-info">
		<div class="panel-body">

			<div class="table-responsive">
				<div class="panel panel-warning">
					<div class="panel-body">
						<form action="<?php echo site_url('buku/cariData') ?>" method="post">
						<div class="col-sm-4 pull-right">
							<div class="form-group pull-right">
							  <div class="input-group">

							  </div>
							</div>
						</div>
						</form>
						<table class="table table-striped">
							<thead>
								<tr>
									<th>Cover</th>
									<th>Kode</th>
									<th>Judul</th>
									<th>Kategori</th>
									<th>Pengarang</th>
									<th>Deskripsi</th>
									<th>Download</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($buku as $b): ?>
									<tr>
										<td>
											<?php
											//	if($b->cover != "")
												//{
													echo '<img src="'.base_url('assets/img/logobaru.jpg').'" alt="" width="100" height="100">';
											//	}
											//	else{
												//	echo '<img src="'.base_url('assets/img/buku/'.$b->cover).'" alt="" width="100" height="130">';
											//	}
											?>
										</td>
										<td><?php echo $b->kd_buku ?></td>
										<td>
											<?php echo $b->judul ?><br>

											<?php
					if($this->session->userdata('level') == '1'){
				echo '<a href='.site_url('buku/edit/'.$b->kd_buku).'>Edit</a>&nbsp;|&nbsp;
				<a href=# class=hapus kode='.$b->kd_buku.'>Hapus</a>';
				}
				 ?>
										</td>
										<td><?php echo $b->kategori ?></td>
										<td><?php echo $b->pengarang ?></td>
										<td width="300"><?php echo $b->deskripsi ?></td>
										<td>
											<?php
												echo '<a href="'.base_url('assets/img/buku/'.$b->kategori.'/'.$b->cover.'').'" target="_blank"><img src="'.base_url('assets/img/pdf.jpg').'" alt="" width="100" height="130"></a>';
												//$status;
												//if ($b->status == "y") {
												//	$status = "Tersedia";
												//}
												//elseif ($b->status == "n") {
												//	$status = "Tidak tersedia / sedang di pinjam";
												//}
											//	echo $status;
											?>
										</td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>

					</div>
				</div>
			</div>
			<div class="well well-sm">
				<a href="<?php echo site_url('buku') ?>" class="btn btn-danger">Batal / Kembali</a>
			</div>
		</div>
	</div>
</div>

<script>
	$(function(){

		//delay alert
		$('#delay-alert').delay(2000).hide(100);

		//delete buku
		$('.hapus').click(function(){
			var kode = $(this).attr('kode');
			$('#idhapus').val(kode);
			$('#modal-delete').modal('show');
		});

		$('#konfirmasi').click(function(){
			var kode = $("#idhapus").val();

			$.ajax({
				url  : "<?php echo site_url('buku/hapus') ?>",
				type : "POST",
				data : "id_hapus="+kode,
				success : function(html){
					location.reload();
				}
			});
		});
	})
</script>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("_partials/head.php") ?>
</head>
<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
      <?php $this->load->view("_partials/sidebar.php") ?>


        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">
              <?php $this->load->view("_partials/navbar.php") ?>


							<!-- Begin Page Content -->
							<div id="delay-alert">
		<?php
			echo $this->session->flashdata('add_success');
			echo $this->session->flashdata('update_success');
			echo $this->session->flashdata('delete_success');
		?>
	</div>

							                <div class="container-fluid">

							                    <!-- Page Heading -->
							                    <h1 class="h3 mb-2 text-gray-800">Daftar Buku Akses Berbayar</h1>
							                    <p class="mb-4"><!--DataTables is a third party plugin that is used to generate the demo table below.
							                        For more information about DataTables, please visit the <a target="_blank"
							                            href="https://datatables.net">official DataTables documentation</a>.--></p>

							                    <!-- DataTales Example -->
							                    <div class="card shadow mb-4">
							                        <div class="card-header py-3">
							                            <h6 class="m-0 font-weight-bold text-primary"></h6>
							                        </div>
							                        <div class="card-body">
							                            <div class="table-responsive">
							                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
							                                    <thead>
							                                        <tr>
                                                                    <th>Cover Buku</th>
							                                        <th>Kode Buku</th>
							                                        <th>Judul Buku</th>
							                                        <th>Kategori</th>
							                                        <th>Penerbit</th>
							                                        <th>Pengarang</th>
							                                        <th>Tgl Input</th>
                                                                    <th>Berbayar</th>
                                                                    <th>Action</th>
							                                        </tr>
							                                    </thead>
							                                    <tfoot>
																<tr>
                                                                <th>Cover Buku</th>
							                                        <th>Kode Buku</th>
							                                        <th>Judul Buku</th>
							                                        <th>Kategori</th>
							                                        <th>Penerbit</th>
							                                        <th>Pengarang</th>
							                                        <th>Tgl Input</th>
                                                                    <th>Berbayar</th>
                                                                    <th>Action</th>
							                                        </tr>
							                                    </tfoot>
							                                    <tbody>
																<?php foreach ($berbayar as $bb): ?>
                                                                    <tr>
																	<td>
																	<?php
												if($bb->cover == "")
												{
													echo '<img src="'.base_url('assets/img/book.png').'" alt="" width="70" height="70">';
												}
												else{
													echo '<img src="'.base_url('assets/buku/'.$bb->kode.'/'.$bb->cover).'" alt="" width="70" height="70">';
												}
											?>
																	</td> 	
																		<td><?php echo $bb->kd_buku ?></td>
							                                            <td><?php echo $bb->judul ?></td>
							                                            <td><?php echo $bb->kategori ?></td>
                                                                        <td><?php echo $bb->penerbit ?></td>
                                                                        <td><?php echo $bb->pengarang ?></td>
							                                            <td><?php echo $bb->tgl_masuk ?></td>
                                                                        <td><?php echo $bb->berbayar ?></td>
                                                                        <td><a href="<?php echo site_url('buku/request/'.$bb->kd_buku) ?>">Request</a>&nbsp;</td>
							                                        </tr>

																											<?php endforeach ?>
							                                    </tbody>
							                                </table>
							                            </div>
							                        </div>
							                    </div>

							                </div>
							                <!-- /.container-fluid -->

							            </div>
							            <!-- End of Main Content -->
																			

                <?php $this->load->view("_partials/footer.php") ?>
            </div>
            <!-- End of Main Content -->



        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <?php $this->load->view("_partials/scrolltop.php") ?>
    <?php $this->load->view("_partials/modal.php") ?>
    <?php $this->load->view("_partials/js.php") ?>




</body>

</html>
</div>
<script>
function deleteConfirm(url){
	$('#btn-delete').attr('href', url);
	$('#deleteModal').modal();
}
</script>
<?php foreach ($buku as $b): ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("_partials/head.php") ?>
</head>
<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
      <?php $this->load->view("_partials/sidebar.php") ?>


        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
              <?php $this->load->view("_partials/navbar.php") ?>


							<!-- Begin Page Content -->
                            
							                <div class="container-fluid">

							                    <!-- Page Heading -->
							                    <h1 class="h3 mb-2 text-gray-800">Detail Buku <?php echo $b->judul ?></h1>
							                    <p class="mb-4"><!--DataTables is a third party plugin that is used to generate the demo table below.
							                        For more information about DataTables, please visit the <a target="_blank"
							                            href="https://datatables.net">official DataTables documentation</a>.--></p>

							                    <!-- DataTales Example -->
                                                <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div></div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <!--<h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>-->
                            </div>
                           
                            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                            <div class="form-group row">
                                
                                    <div class="col-sm-6">
                                    
                                    </div>
                                </div>
                                <div>
                                
                                <?php echo '<iframe src="'.base_url('assets/buku/'.$b->kategori.'/'.$b->file_ebook.'').'" style="cursor:pointer" width="1150" height="1300"></embed>';?></iframe>   
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                               
                                    </div>
                                   
                                </div>
                               
                                
                               <a href="<?php echo site_url('buku/view/'.$b->kd_buku)?>" class="btn btn-primary">Kembali</a>  
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
							                <!-- /.container-fluid -->

							            </div>
                                        
							            <!-- End of Main Content -->
                                                       
                <?php $this->load->view("_partials/footer.php") ?>
            </div>
            <!-- End of Main Content -->



        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <?php $this->load->view("_partials/scrolltop.php") ?>
    <?php $this->load->view("_partials/modal.php") ?>
    <?php $this->load->view("_partials/js.php") ?>




</body>

</html>
<?php endforeach ?>
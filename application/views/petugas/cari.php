<div id="buku">
	
	<div class="panel panel-info">
		<div class="panel-body">
			
			<div class="table-responsive">
				<div class="panel panel-warning">
					<div class="panel-body">
						<form action="<?php echo site_url('petugas/cariData') ?>" method="post">	
						<div class="col-sm-4 pull-right">
							<div class="form-group pull-right">
							  <div class="input-group">
							    
							  </div>
							</div>
						</div>
						</form>
						<table class="table table-striped">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama</th>
									<th>Username</th>
								</tr>
							</thead>
							<tbody>
								<?php $no=1; foreach ($petugas as $p): ?>
									<tr>
										<td><?php echo $no++; ?></td>
										<td>
											<?php echo $p->nama ?><br>
											<a href="<?php echo site_url('petugas/edit/'.$p->id_user) ?>">Edit</a>&nbsp;|&nbsp;
											<a href="#" class="hapus" kode="<?php echo $p->id_user ?>">Hapus</a>&nbsp;|&nbsp;
											<a href="#" class="reset_pass" kode="<?php echo $p->id_user ?>" pass="<?php echo $p->username ?>">Reset Password </a>
										</td>
										<td width="500"><?php echo $p->username ?></td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
						
					</div>
				</div>
			</div>	
			<div class="well well-sm">
				<a href="<?php echo site_url('buku') ?>" class="btn btn-danger">Batal / Kembali</a>
			</div>
		</div>
	</div>
</div>

<script>
	$(function(){

		//delay alert
		$('#delay-alert').delay(2000).hide(100);

		//delete buku
		$('.hapus').click(function(){
			var kode = $(this).attr('kode');
			$('#idhapus').val(kode);
			$('#modal-delete').modal('show');
		});

		$('#konfirmasi').click(function(){
			var kode = $("#idhapus").val();

			$.ajax({
				url  : "<?php echo site_url('buku/hapus') ?>",
				type : "POST",
				data : "id_hapus="+kode,
				success : function(html){
					location.reload();
				} 
			});
		});
	})
</script>
<!-- modal fade -->
<div class="modal fade" id="modal-delete">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<p>Delete Data</p>
					</div>
					<div class="modal-body">
						<input type="hidden" name="idhapus" id="idhapus">
						<p>Apakah anda yakin ingin menghapus data ini?</p>
					</div>
					<div class="modal-footer">
						<button id="konfirmasi" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</button>
						<button class="btn btn-info" data-dismiss="modal">Tutup</button>
					</div>
				</div>
			</div>
		</div>

<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; E-library SMP Angkasa Halim Perdanakusuma Jakarta 2021</span>
        </div>
    </div>
</footer>
<!-- End of Footer -->

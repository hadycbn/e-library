<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>
<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
      <?php $this->load->view("admin/_partials/sidebar.php") ?>


        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">
              <?php $this->load->view("admin/_partials/navbar.php") ?>


                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
                        <!--<a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                                class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>-->
                    </div>

                    <!-- Content Row -->
                    <div class="row">

                        <!-- Statistik Buku -->
                        <div class="col-lg-12">
                            <div class="card mb-4">
                                <div class="card-header">
                                    Statistik Buku
                                </div>
                                <div class="card-body">
                                    <div class="row">

                                        <!-- Jumlah Buku Gratis -->
                                        <div class="col-lg-3 mb-4">
                                            <div class="card border-left-primary shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                                Jumlah Buku Gratis</div>
                                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $statistik_buku['jumlah_buku_gratis'] ?></div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <i class="fas fa-book fa-2x text-gray-300"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Jumlah Buku Berbayar -->
                                        <div class="col-lg-3 mb-4">
                                            <div class="card border-left-success shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                                Jumlah Buku Berbayar</div>
                                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $statistik_buku['jumlah_buku_berbayar'] ?></div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <i class="fas fa-book fa-2x text-gray-300"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Jumlah Buku Di Baca -->
                                        <div class="col-lg-3 mb-4">
                                            <div class="card border-left-info shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                                                                Jumlah Buku Di Baca</div>
                                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $statistik_buku['jumlah_buku_dibaca'] ?></div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <i class="fas fa-book fa-2x text-gray-300"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Jumlah Buku Di Download -->
                                        <div class="col-lg-3 mb-4">
                                            <div class="card border-left-warning shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                                                Jumlah Buku Di Download</div>
                                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $statistik_buku['jumlah_buku_didownload'] ?></div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <i class="fas fa-book fa-2x text-gray-300"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Jumlah Total Buku -->
                                        <div class="col-lg-3 mb-4">
                                            <div class="card border-left-danger shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
                                                                Jumlah Total Buku</div>
                                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $statistik_buku['jumlah_total_buku'] ?></div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <i class="fas fa-book fa-2x text-gray-300"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Chart Top 5 Buku -->
                                        <div class="col-lg-12">
                                            <div class="card shadow">
                                                <!-- Card Header - Dropdown -->
                                                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                                    <h6 class="m-0 font-weight-bold text-primary">Top 5 Buku Paling Banyak Dibaca</h6>
                                                </div>
                                                <!-- Card Body -->
                                                <div class="card-body">
                                                    <div class="chart-pie pt-4 pb-2">
                                                        <canvas id="myPieChart"></canvas>
                                                    </div>
                                                    <div class="mt-4 text-center small">
                                                        <span class="mr-2">
                                                            <i class="fas fa-circle text-primary"></i> Direct
                                                        </span>
                                                        <span class="mr-2">
                                                            <i class="fas fa-circle text-success"></i> Social
                                                        </span>
                                                        <span class="mr-2">
                                                            <i class="fas fa-circle text-info"></i> Referral
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Content Row -->
                    <div class="row">

                        <!-- Statistik Pengunjung -->
                        <div class="col-lg-12">
                            <div class="card mb-4">
                                <div class="card-header">
                                    Statistik Pengunjung
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        
                                        <!-- Pengungjung Online -->
                                        <div class="col-lg-3 mb-4">
                                            <div class="card border-left-primary shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                                Pengungjung Online</div>
                                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $statistik_pengunjung['jumlah_pengunjung_online'] ?></div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <i class="fas fa-users fa-2x text-gray-300"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Pengungjung Hari Ini -->
                                        <div class="col-lg-3 mb-4">
                                            <div class="card border-left-success shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                                Pengungjung Hari Ini</div>
                                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $statistik_pengunjung['jumlah_pengunjung_hari_ini'] ?></div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <i class="fas fa-users fa-2x text-gray-300"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Pengungjung Bulan Ini -->
                                        <div class="col-lg-3 mb-4">
                                            <div class="card border-left-info shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                                                                Pengungjung Bulan Ini</div>
                                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $statistik_pengunjung['jumlah_pengunjung_bulan_ini'] ?></div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <i class="fas fa-users fa-2x text-gray-300"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Total Pengungjung -->
                                        <div class="col-lg-3 mb-4">
                                            <div class="card border-left-warning shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                                                Total Pengungjung</div>
                                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $statistik_pengunjung['jumlah_total_pengunjung'] ?></div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <i class="fas fa-users fa-2x text-gray-300"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Grafik Pengunjung -->
                                        <div class="col-lg-12">
                                            <div class="card shadow">
                                                <!-- Card Header - Dropdown -->
                                                <div
                                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                                    <h6 class="m-0 font-weight-bold text-primary">Grafik Pengunjung</h6>
                                                </div>
                                                <!-- Card Body -->
                                                <div class="card-body">
                                                    <div class="chart-area">
                                                        <canvas id="myAreaChart"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Statistik Anggota -->
                        <div class="col-lg-12">
                            <div class="card mb-4">
                                <div class="card-header">
                                    Statistik Anggota
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        
                                        <!-- Anggota Baru Hari ini -->
                                        <div class="col-lg-3 mb-4">
                                            <div class="card border-left-primary shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                                Anggota Baru Hari ini</div>
                                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $statistik_anggota['jumlah_anggota_hari_ini'] ?></div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <i class="fas fa-users fa-2x text-gray-300"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Anggota Baru Minggu ini -->
                                        <div class="col-lg-3 mb-4">
                                            <div class="card border-left-success shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                                Anggota Baru Minggu ini</div>
                                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $statistik_anggota['jumlah_anggota_minggu_ini'] ?></div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <i class="fas fa-users fa-2x text-gray-300"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Anggota Baru Bulan ini -->
                                        <div class="col-lg-3 mb-4">
                                            <div class="card border-left-info shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                                                                Anggota Baru Bulan ini</div>
                                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $statistik_anggota['jumlah_anggota_bulan_ini'] ?></div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <i class="fas fa-users fa-2x text-gray-300"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Jumlah Total Anggota -->
                                        <div class="col-lg-3 mb-4">
                                            <div class="card border-left-warning shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                                                Jumlah Total Anggota</div>
                                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $statistik_anggota['jumlah_total_anggota'] ?></div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <i class="fas fa-users fa-2x text-gray-300"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Grafik Anggota -->
                                        <div class="col-lg-12">
                                            <div class="card shadow">
                                                <!-- Card Header - Dropdown -->
                                                <div
                                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                                    <h6 class="m-0 font-weight-bold text-primary">Grafik Anggota</h6>
                                                </div>
                                                <!-- Card Body -->
                                                <div class="card-body">
                                                    <div class="chart-area">
                                                        <canvas id="myAreaChart"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    <!-- Content Row -->
                    <div class="row">

                        <!-- Content Column -->
                        

                        
                    </div>

                </div>
                <!-- /.container-fluid -->
                <?php $this->load->view("admin/_partials/footer.php") ?>
            </div>
            <!-- End of Main Content -->



        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <?php $this->load->view("admin/_partials/scrolltop.php") ?>
    <?php $this->load->view("admin/_partials/modal.php") ?>
    <?php $this->load->view("admin/_partials/js.php") ?>




</body>

</html>

<div class="table-responsive">
<h6><a href="<?php echo site_url('admin/lap_anggota/excel'); ?>">Export ke Excel</a></h6>
	<table class="table table-bordered">
		<thead>
			<tr>
			<th>ID Anggota</th>
			<th>Nama</th>
			<th>Email</th>
			<th>No HP</th>
            <th>Tanggal Daftar</th>
            <th>Status</th>
			</tr>
		</thead>
		<tbody>
			<?php $no = 1; foreach ($tampil_data as $d): ?>
				<tr>
					<td><?php echo $d->id_anggota ?></td>
					<td><?php echo $d->nama ?></td>
					<td><?php echo $d->email ?></td>
					<td><?php echo $d->hp ?></td>
					<td><?php echo $d->tgl_daftar ?></td>
					<td>
					<?php
					if($d->status == "Y")
												{
													echo 'Aktif';
												}
												else{
													echo 'Tidak Aktif';
												}
					?>
												</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</div>
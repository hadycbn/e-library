<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>
<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
      <?php $this->load->view("admin/_partials/sidebar.php") ?>


        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">
              <?php $this->load->view("admin/_partials/navbar.php") ?>


							<!-- Begin Page Content -->
							                <div class="container-fluid">

							                    <!-- Page Heading -->
							                    <h1 class="h3 mb-2 text-gray-800">Laporan Data Anggota</h1>
							                    <p class="mb-4"><!--DataTables is a third party plugin that is used to generate the demo table below.
							                        For more information about DataTables, please visit the <a target="_blank"
							                            href="https://datatables.net">official DataTables documentation</a>.--></p>

							                    <!-- DataTales Example -->
                                                <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div></div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                            
                            </div>
                            <script>
	
	// jquery
	$(function() {

		//tgl
		$("#tgl1").datepicker({
			format : "yyyy-mm-dd"
		});
		$('#tgl2').datepicker({
			format : "yyyy-mm-dd"
		});

		$("#cari").click(function(){
			var tgl1 = $("#tgl1").val();
			var tgl2 = $("#tgl2").val();

			$.ajax({
				url : "<?php echo site_url('admin/lap_anggota/get_report') ?>",
				type : "POST",
				data : {tgl1 : tgl1, tgl2 : tgl2},
				beforeSend : function(msg){
					$("#tampil").html('<div class="alert alert-success"><i class="fa fa-spinner fa-spin"></i> Sedang memproses data ...</div>');
				},
				success : function(msg){
					if (msg == "") {
						$("#tampil").html('<tr><td colspan="5" align="center">Data tidak ada ... !!!</td></tr>');
					}
					else
					{
						$("#tampil").html(msg);
					}
				}
			})
		});

	})
</script>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                   Start Date
                                    </div>
                                    <div class="col-sm-3">
                                    <input type="text" class="form-control datepicker" id="tgl1" value="<?php echo $this->input->post('tgl1') ?>">
                                   
                                    </div>
                                </div>
                                <script type="text/javascript">
        $(function(){
            $(".datepicker").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayHighlight: true,
            });
        });
    </script>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                   End Date
                                    </div>
                                    <div class="col-sm-3">
                                    <input type="text" class="form-control datepicker" id="tgl2" value="<?php echo $this->input->post('tgl2') ?>">
						            
                                    </div>
                                </div>
                               
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                         
                                    </div>
                                </div>
                                <button class="btn btn-primary" id="cari">Tampilkan Laporan</button>&nbsp;
                               
                              

                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12"><hr></div>
			<div class="col-lg-12">
				<div id="loading"></div>
				<div id="tampil">
					<table class="table table-bordered" width="100%">
						<thead>
							<tr>
								<th>No</th>
								<th>ID Anggota</th>
								<th>Nama</th>
								<th>Email</th>
								<th>No HP</th>
                                <th>Tanggal Daftar</th>
                                <th>Status</th>
							</tr>
						</thead>
						<tbody id="kosong">
							
						</tbody>
					</table>
				</div>
			</div>  
        </div>
        
							                <!-- /.container-fluid -->

							            </div>
							            <!-- End of Main Content -->

                <?php $this->load->view("admin/_partials/footer.php") ?>
            </div>
            <!-- End of Main Content -->



        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <?php $this->load->view("admin/_partials/scrolltop.php") ?>
    <?php $this->load->view("admin/_partials/modal.php") ?>
    <?php $this->load->view("admin/_partials/js.php") ?>




</body>

</html>
<script>
	$(function(){
		$('.alert-delay').delay(2000).fadeOut(2000);
	});
</script>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>
<body class="bg-gradient-primary">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block"><img src="<?php echo base_url('assets/img/img-20210123-wa0021.jpg')?>" width="489" height="350"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Welcome Administrator!</h1>
                                        
                                    </div>
                                    <div class="delay-alert">
										<?php echo $this->session->flashdata('login_fail'); ?>
                  </div>
                  <form action="" method="post">
                  <div class="form-group">
                                            <input type="email" class="form-control form-control-user" name="username"
                                                placeholder="Enter Username..."><?php echo form_error('username') ?>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-user" name="password"
                                                id="exampleInputPassword" placeholder="Password"><?php echo form_error('password') ?>
                                        </div>
                    <div class="row px-3 mb-4">
                        <div class="custom-control custom-checkbox custom-control-inline">

                           </div>
                            <a href="#" class="ml-auto mb-0 text-sm">Forgot Password?</a>
                    </div>
                    <div class="row px-3"> <button type="submit" class="btn btn-primary btn-user btn-block">Login</button></div>
                    
									</form>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>


    <script type='text/javascript' src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js'></script>
                                <script type='text/javascript'></script>
</body>

</html>
<script>
															$(function(){
																$('.delay-alert').delay(2000).hide(100);
															})
														</script>
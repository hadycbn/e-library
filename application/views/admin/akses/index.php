<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>
<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
      <?php $this->load->view("admin/_partials/sidebar.php") ?>


        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">
              <?php $this->load->view("admin/_partials/navbar.php") ?>


							<!-- Begin Page Content -->
							<div id="delay-alert">
		<?php
			echo $this->session->flashdata('add_success');
			echo $this->session->flashdata('update_success');
			echo $this->session->flashdata('delete_success');
		?>
	</div>

							                <div class="container-fluid">

							                    <!-- Page Heading -->
							                    <h1 class="h3 mb-2 text-gray-800">Daftar Permintaan Akses Buku Berbayar</h1>
							                    <p class="mb-4"><!--DataTables is a third party plugin that is used to generate the demo table below.
							                        For more information about DataTables, please visit the <a target="_blank"
							                            href="https://datatables.net">official DataTables documentation</a>.--></p>

							                    <!-- DataTales Example -->
							                    <div class="card shadow mb-4">
							                        <div class="card-header py-3">
							                            <h6 class="m-0 font-weight-bold text-primary"><a href="akses/tambah">Tambah Data Akses Buku Berbayar</a></h6>
							                        </div>
							                        <div class="card-body">
							                            <div class="table-responsive">
							                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
							                                    <thead>
							                                        <tr>
                                                                    <th>ID Anggota</th>
                                                                    <th>Nama</th>
                                                                    <th>Kode Buku</th>
                                                                    <th>Judul Buku</th>
																	<th>Tgl Submit</th>
																	<th>Tgl Expired</th>
                                                                    <th>Status</th>
							                                        <th>Action</th>
							                                        </tr>
							                                    </thead>
							                                    <tfoot>
																<tr>
                                                                    <th>ID Anggota</th>
                                                                    <th>Nama</th>
                                                                    <th>Kode Buku</th>
                                                                    <th>Judul Buku</th>
																	<th>Tgl Submit</th>
																	<th>Tgl Expired</th>
                                                                    <th>Status</th>
							                                        <th>Action</th>
							                                        </tr>
							                                    </tfoot>
							                                    <tbody>
																<?php foreach ($akses as $ak): ?>
							                                        <tr>	
                                                                    <td><?php echo $ak->id_anggota ?></td>
                                                                    <td><?php echo $ak->nama ?>
                                                                    <td><?php echo $ak->kd_buku ?>
																	<td><?php echo $ak->judul ?></td>
																	<td><?php echo $ak->tgl_submit ?>
                                                                    <td><?php echo $ak->tgl_expired ?>
                                                                    <td>
                                                                    <?php
												if($ak->status == "Y")
												{
													echo 'Approved';
												}
												else{
													echo 'Request';
												}
											?></td>
							                                            <td align="center">
																		
<a href="<?php echo site_url('admin/akses/edit/'.$ak->id) ?>"><img src="<?php echo base_url('assets/img/edit.png')?>"></a>&nbsp;
<a onclick="deleteConfirm('<?php echo site_url('admin/akses/delete/'.$ak->id) ?>')" href="#!" class="btn btn-small text-danger"><img src="<?php echo base_url('assets/img/delete.png')?>"></a>&nbsp;
											
																		
																		</td>
							                                        </tr>

																											<?php endforeach ?>
							                                    </tbody>
							                                </table>
							                            </div>
							                        </div>
							                    </div>

							                </div>
							                <!-- /.container-fluid -->

							            </div>
							            <!-- End of Main Content -->
																			

                <?php $this->load->view("admin/_partials/footer.php") ?>
            </div>
            <!-- End of Main Content -->



        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <?php $this->load->view("admin/_partials/scrolltop.php") ?>
    <?php $this->load->view("admin/_partials/modal.php") ?>
    <?php $this->load->view("admin/_partials/js.php") ?>




</body>

</html>
</div>
<script>
function deleteConfirm(url){
	$('#btn-delete').attr('href', url);
	$('#deleteModal').modal();
}
</script>
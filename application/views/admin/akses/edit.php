<?php foreach ($akses as $ak): ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>
<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
      <?php $this->load->view("admin/_partials/sidebar.php") ?>


        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">
              <?php $this->load->view("admin/_partials/navbar.php") ?>


							<!-- Begin Page Content -->
							                <div class="container-fluid">

							                    <!-- Page Heading -->
							                    <h1 class="h3 mb-2 text-gray-800">Edit Permintaan Akses</h1>
							                    <p class="mb-4"><!--DataTables is a third party plugin that is used to generate the demo table below.
							                        For more information about DataTables, please visit the <a target="_blank"
							                            href="https://datatables.net">official DataTables documentation</a>.--></p>

							                    <!-- DataTales Example -->
                                                <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div></div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <!--<h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>-->
                            </div>
                            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                    ID Anggota
                                    </div>
                                    <div class="col-sm-6">
                                    <input type="text" name="nama" class="form-control form-control-user" value="<?php echo $ak->id_anggota ?>" readonly>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                    Nama Anggota
                                    </div>
                                    <div class="col-sm-6">
                                    <input type="text" name="nama" class="form-control form-control-user" value="<?php echo $ak->nama ?>"readonly>
						           
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                    Kode Buku
                                    </div>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control form-control-user" name="email" value="<?php echo $ak->kd_buku ?>"readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    Judul Buku
                                    </div>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control form-control-user" name="email" value="<?php echo $ak->judul ?>"readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                   Tgl Submit
                                    </div>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control form-control-user" name="email" value="<?php echo $ak->tgl_submit ?>"readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    Tgl Expired
                                    </div>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control form-control-user" name="email" value="<?php echo $ak->tgl_expired ?>"readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                   Status Pengajuan
                                    </div>
                                    <div class="col-sm-6">
                                    <select name="status" class="form-control">
							<option value="">--Pilih Level--</option>
							<?php
								$array = array('Y' =>'Approved', 'N' => 'Request');
								foreach ($array as $value => $text) {
									$selected = ($value == $ak->status)?"selected":"";
									echo '<option value="'.$value.'" '.$selected.'>'.ucwords($text).'</option>';
								}
							?>
						</select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                         
                                    </div>
                                </div>
                               <button class="btn btn-primary">Simpan Perubahan</button>&nbsp;
                               <a href="<?php echo site_url('admin/akses')?>" class="btn btn-primary">
                                    Kembali
                                </a>
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
							                <!-- /.container-fluid -->

							            </div>
							            <!-- End of Main Content -->

                <?php $this->load->view("admin/_partials/footer.php") ?>
            </div>
            <!-- End of Main Content -->



        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <?php $this->load->view("admin/_partials/scrolltop.php") ?>
    <?php $this->load->view("admin/_partials/modal.php") ?>
    <?php $this->load->view("admin/_partials/js.php") ?>




</body>

</html>
<?php endforeach ?>
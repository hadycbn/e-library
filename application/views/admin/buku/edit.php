<?php foreach ($buku as $b): ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>
<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
      <?php $this->load->view("admin/_partials/sidebar.php") ?>


        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
              <?php $this->load->view("admin/_partials/navbar.php") ?>


							<!-- Begin Page Content -->
                            
							                <div class="container-fluid">

							                    <!-- Page Heading -->
							                    <h1 class="h3 mb-2 text-gray-800">Edit Buku</h1>
							                    <p class="mb-4"><!--DataTables is a third party plugin that is used to generate the demo table below.
							                        For more information about DataTables, please visit the <a target="_blank"
							                            href="https://datatables.net">official DataTables documentation</a>.--></p>

							                    <!-- DataTales Example -->
                                                <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div></div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <!--<h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>-->
                            </div>
                           
                            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        Kode Buku
                                    </div>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control form-control-user" name="kd_buku"
                                            placeholder="First Name" value="<?php echo $b->kd_buku ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                    Kategori Buku
                                    </div>
                                    <div class="col-sm-6">
                                    <select class="form-control" name="kategori">
                                    <?php 

foreach ($user as $l)
 {
 ?>
   <option  <?php if($b->kategori == $l->kategori ) echo "selected";?> value="<?php echo $l->kode?>">
  <?php echo $l->kategori;?> </option>

<?php
 }
?>
					        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                    Judul Buku
                                    </div>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control form-control-user" name="judul" value="<?php echo $b->judul ?>"
                                        placeholder="Input Judul Buku">
                                        <?php echo form_error('judul') ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    Penerbit Buku
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-user" name="penerbit"
                                            placeholder="Input Penerbit Buku" value="<?php echo $b->penerbit ?>">
                                            <?php echo form_error('penerbit') ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    Pengarang Buku
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-user" name="pengarang"
                                            placeholder="Input Pengarang Buku" value="<?php echo $b->pengarang ?>">
                                            <?php echo form_error('pengarang') ?>
                                    </div>
                                </div>
                                <div class="form-groupv row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    Deskripsi Buku
                                    </div>
                                    <div class="col-sm-12">
                                <textarea class="form-control form-control-user" name="deskripsi"  rows="3" >
                                <?php echo $b->deskripsi ?></textarea>
                                            <?php echo form_error('deskripsi') ?>
                                </div>
                                </div>
                                <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    Akses Buku
                                    </div>
                                    <div class="col-sm-6">
                                    <select name="berbayar" class="form-control">
							
							<?php  
								$array = array('Y' =>'AKSES BERBAYAR', 'N' => 'AKSES GRATIS');
								foreach ($array as $value => $text) {
									$selected = ($value == $b->berbayar)?"selected":"";
									echo '<option value="'.$value.'" '.$selected.'>'.ucwords($text).'</option>';
								}
							?>
						</select>
						
                                    </div>
                                </div>
                                <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    Status Buku
                                    </div>
                                    <div class="col-sm-6">
                                    <select name="status" class="form-control">
							<?php  
								$array = array('Y' =>'AKTIF', 'N' => 'TIDAK AKTIF');
								foreach ($array as $value => $text) {
									$selected = ($value == $b->status)?"selected":"";
									echo '<option value="'.$value.'" '.$selected.'>'.ucwords($text).'</option>';
								}
							?>
						</select>
						
                                    </div>
                                </div>
                                <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                Cover E-Book
                                    </div>
                                    <div class="col-sm-6">
                                    <?php
												if($b->cover == "")
												{
													echo '<img src="'.base_url('assets/img/book.png').'" alt="" width="120" height="120">';
												}
												else{
													echo '<img src="'.base_url('assets/buku/'.$b->kode.'/'.$b->cover).'" alt="" width="120" height="140">';
												}
											?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                File E-Book
                                    </div>
                                    <div class="col-sm-6">
                                    
                                    </div>
                                </div>
                                <div>
                                
                                <?php echo '<iframe src="'.base_url('assets/buku/'.$b->kode.'/'.$b->file_ebook.'').'" style="cursor:pointer" width="1000" height="800"></embed>';?></iframe>   
                            </div>
                               <button class="btn btn-primary">Simpan Perubahan</button>&nbsp;&nbsp;
                               <a href="<?php echo site_url('admin/buku')?>" class="btn btn-primary">
                                    Kembali
                                </a>
                                  
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
							                <!-- /.container-fluid -->

							            </div>
                                        
							            <!-- End of Main Content -->
                                                       
                <?php $this->load->view("admin/_partials/footer.php") ?>
            </div>
            <!-- End of Main Content -->



        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <?php $this->load->view("admin/_partials/scrolltop.php") ?>
    <?php $this->load->view("admin/_partials/modal.php") ?>
    <?php $this->load->view("admin/_partials/js.php") ?>




</body>

</html>
<?php endforeach ?>
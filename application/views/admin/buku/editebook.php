<?php foreach ($buku as $b): ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>
<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
      <?php $this->load->view("admin/_partials/sidebar.php") ?>


        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
              <?php $this->load->view("admin/_partials/navbar.php") ?>


							<!-- Begin Page Content -->
                            
							                <div class="container-fluid">

							                    <!-- Page Heading -->
							                    <h1 class="h3 mb-2 text-gray-800">Edit File E-Book</h1>
							                    <p class="mb-4"><!--DataTables is a third party plugin that is used to generate the demo table below.
							                        For more information about DataTables, please visit the <a target="_blank"
							                            href="https://datatables.net">official DataTables documentation</a>.--></p>

							                    <!-- DataTales Example -->
                                                <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div></div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <!--<h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>-->
                            </div>
                           
                            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        Kode Buku
                                    </div>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control form-control-user" name="kd_buku"
                                            placeholder="First Name" value="<?php echo $b->kd_buku ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                    Kategori Buku
                                    </div>
                                    <div class="col-sm-6">
                                    <select class="form-control" name="kategori">
                                    <?php 

foreach ($user as $l)
 {
 ?>
   <option  <?php if($b->kategori == $l->kategori ) echo "selected";?> value="<?php echo $l->kode?>">
  <?php echo $l->kategori;?> </option>

<?php
 }
?>
					        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    File E-Book
                                    </div>
                                    <div class="col-sm-6">
                                    <input type="file" name="ebook" class="form-control" accept=".pdf" />
                                    <?php echo form_error('ebook') ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    Cover E-Book
                                    </div>
                                    <div class="col-sm-6">
                                    <input type="file" name="cover" class="form-control" accept="image/*" />
                                    <?php echo form_error('cover') ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                         
                                    </div>
                                </div>
                                <button class="btn btn-primary">Simpan Perubahan</button>&nbsp;&nbsp;
                               <a href="<?php echo site_url('admin/buku/index')?>" class="btn btn-primary">
                                    Kembali
                                </a>
                                
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
							                <!-- /.container-fluid -->

							            </div>
                                        
							            <!-- End of Main Content -->
                                                       
                <?php $this->load->view("admin/_partials/footer.php") ?>
            </div>
            <!-- End of Main Content -->



        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <?php $this->load->view("admin/_partials/scrolltop.php") ?>
    <?php $this->load->view("admin/_partials/modal.php") ?>
    <?php $this->load->view("admin/_partials/js.php") ?>




</body>

</html>
<?php endforeach ?>
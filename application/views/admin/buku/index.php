<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>
<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
      <?php $this->load->view("admin/_partials/sidebar.php") ?>


        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">
              <?php $this->load->view("admin/_partials/navbar.php") ?>


							<!-- Begin Page Content -->
							<div id="delay-alert">
		<?php
			echo $this->session->flashdata('add_success');
			echo $this->session->flashdata('update_success');
			echo $this->session->flashdata('delete_success');
		?>
	</div>

							                <div class="container-fluid">

							                    <!-- Page Heading -->
							                    <h1 class="h3 mb-2 text-gray-800">Daftar Buku</h1>
							                    <p class="mb-4"><!--DataTables is a third party plugin that is used to generate the demo table below.
							                        For more information about DataTables, please visit the <a target="_blank"
							                            href="https://datatables.net">official DataTables documentation</a>.--></p>

							                    <!-- DataTales Example -->
							                    <div class="card shadow mb-4">
							                        <div class="card-header py-3">
							                            <h6 class="m-0 font-weight-bold text-primary"><a href="buku/tambah">Tambah Data Buku</a></h6>
							                        </div>
							                        <div class="card-body">
							                            <div class="table-responsive">
							                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
							                                    <thead>
							                                        <tr>
																		<th>Cover Buku</th>
							                                            <th>Kode Buku</th>
							                                            <th>Judul Buku</th>
							                                            <th>Kategori</th>
							                                            <th>Tgl Input</th>
							                                            <th>Berbayar</th>
																		<th>Aktif</th>
							                                            <th>Action</th>
							                                        </tr>
							                                    </thead>
							                                    <tfoot>
							                                        <tr>
																		<th>Cover Buku</th>
							                                            <th>Kode Buku</th>
							                                            <th>Judul Buku</th>
							                                            <th>Kategori</th>
							                                            <th>Tgl Input</th>
							                                            <th>Berbayar</th>
																		<th>Aktif</th>
							                                            <th>Action</th>
							                                        </tr>
							                                    </tfoot>
							                                    <tbody>
																										<?php foreach ($buku as $b): ?>
							                                        <tr>
																	<td>
																	<?php
												if($b->cover == "")
												{
													echo '<img src="'.base_url('assets/img/book.png').'" alt="" width="70" height="70">';
												}
												else{
													echo '<img src="'.base_url('assets/buku/'.$b->kode.'/'.$b->cover).'" alt="" width="70" height="70">';
												}
											?>
																	</td> 	
																		<td><?php echo $b->kd_buku ?></td>
							                                            <td><?php echo $b->judul ?></td>
							                                            <td><?php echo $b->kategori ?></td>
							                                            <td><?php echo $b->tgl_masuk ?></td>
							                                            <td><?php echo $b->berbayar ?></td>
																		<td><?php echo $b->status ?></td>
							                                            <td>
																		
<a href="<?php echo site_url('admin/buku/edit/'.$b->kd_buku) ?>"><img src="<?php echo base_url('assets/img/edit.png')?>"></a>&nbsp;
<a href="<?php echo site_url('admin/buku/editebook/'.$b->kd_buku) ?>"><img src="<?php echo base_url('assets/img/doc.png')?>"></a>&nbsp;
<a onclick="deleteConfirm('<?php echo site_url('admin/buku/delete/'.$b->kd_buku) ?>')" href="#!" class="btn btn-small text-danger"><img src="<?php echo base_url('assets/img/delete.png')?>"></a>

											
																		
																		</td>
							                                        </tr>

																											<?php endforeach ?>
							                                    </tbody>
							                                </table>
							                            </div>
							                        </div>
							                    </div>

							                </div>
							                <!-- /.container-fluid -->

							            </div>
							            <!-- End of Main Content -->
																			

                <?php $this->load->view("admin/_partials/footer.php") ?>
            </div>
            <!-- End of Main Content -->



        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <?php $this->load->view("admin/_partials/scrolltop.php") ?>
    <?php $this->load->view("admin/_partials/modal.php") ?>
    <?php $this->load->view("admin/_partials/js.php") ?>




</body>

</html>
</div>
<script>
function deleteConfirm(url){
	$('#btn-delete').attr('href', url);
	$('#deleteModal').modal();
}
</script>
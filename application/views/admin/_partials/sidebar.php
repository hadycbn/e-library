<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo site_url('admin/overview') ?>">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-6">E-library <br>SMP Angkasa</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="<?php echo site_url('admin/overview') ?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading 
    <div class="sidebar-heading">
        Interface
    </div>-->

    <!-- Nav Item - Pages Collapse Menu -->
    <?php
        $isManageBookActive = strpos(current_url(), 'admin/buku') !== false ||
            strpos(current_url(), 'admin/buku/tambah') !== false
    ?>
    <li class="nav-item <?= $isManageBookActive ? 'active' : '' ?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
            aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-book-open"></i>
            <span>Buku</span>
        </a>
        <div id="collapseTwo" class="collapse <?= $isManageBookActive ? 'show' : '' ?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Kelola Data Buku:</h6>
                <a class="collapse-item <?= strpos(current_url(), 'admin/buku') !== false ? 'active' : '' ?>" href="<?php echo site_url('admin/buku') ?>">Daftar Buku</a>
                <a class="collapse-item <?= strpos(current_url(), 'admin/buku/tambah') !== false ? 'active' : '' ?>" href="<?php echo site_url('admin/buku/tambah') ?>">Tambah Buku</a>
            </div>
        </div>
    </li>
  <hr class="sidebar-divider">
    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
            aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-user-friends"></i>
            <span>Anggota</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
            data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Kelola Data Anggota:</h6>
                <a class="collapse-item" href="<?php echo site_url('admin/anggota') ?>">Daftar Anggota</a>
                <a class="collapse-item" href="<?php echo site_url('admin/anggota/tambah') ?>">Tambah Data Anggota</a>
            </div>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">
    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseKategori"
            aria-expanded="true" aria-controls="collapseKategori">
            <i class="fas fa-fw fa-book"></i>
            <span>Kategori Buku</span>
        </a>
        <div id="collapseKategori" class="collapse" aria-labelledby="headingUtilities"
            data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Kelola Data Kategori:</h6>
                <a class="collapse-item" href="<?php echo site_url('admin/kategori') ?>">Daftar Kategori</a>
                <a class="collapse-item" href="<?php echo site_url('admin/kategori/tambah') ?>">Tambah Data Kategori</a>
            </div>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading
    <div class="sidebar-heading">
        Addons
    </div>-->

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"
            aria-expanded="true" aria-controls="collapsePages">
            <i class="fas fa-fw fa-user-tie"></i>
            <span>User</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Manage User:</h6>
                <a class="collapse-item" href="<?php echo site_url('admin/petugas') ?>">Daftar User</a>
                <a class="collapse-item" href="<?php echo site_url('admin/petugas/tambah') ?>">Tambah Data User</a>
                <!--<a class="collapse-item" href="login.html">Login</a>
                <a class="collapse-item" href="register.html">Register</a>
                <a class="collapse-item" href="forgot-password.html">Forgot Password</a>
                <div class="collapse-divider"></div>
                <h6 class="collapse-header">Other Pages:</h6>
                <a class="collapse-item" href="404.html">404 Page</a>
                <a class="collapse-item" href="blank.html">Blank Page</a>-->
            </div>
        </div>
    </li>
  <hr class="sidebar-divider">
    <!-- Nav Item - Charts -->
    <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('admin/akses') ?>">
            <i class="fas fa-fw fa-cog"></i>
            <span>Permintaan Akses Buku</span></a>
    </li>
    <hr class="sidebar-divider">
    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseReport"
            aria-expanded="true" aria-controls="collapseReport">
            <i class="fas fa-fw fa-book"></i>
            <span>Report</span>
        </a>
        <div id="collapseReport" class="collapse" aria-labelledby="headingUtilities"
            data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Kelola Report:</h6>
                <a class="collapse-item" href="<?php echo site_url('admin/lap_anggota') ?>">Report Anggota</a>
                <a class="collapse-item" href="<?php echo site_url('admin/laporan/buku') ?>">Report Buku</a>
            </div>
        </div>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

    <!-- Sidebar Message
    <div class="sidebar-card d-none d-lg-flex">
        <img class="sidebar-card-illustration mb-2" src="img/undraw_rocket.svg" alt="...">
        <p class="text-center mb-2"><strong>SB Admin Pro</strong> is packed with premium features, components, and more!</p>
        <a class="btn btn-success btn-sm" href="https://startbootstrap.com/theme/sb-admin-pro">Upgrade to Pro!</a>
    </div>-->

</ul>
<!-- End of Sidebar -->

<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>
<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
      <?php $this->load->view("admin/_partials/sidebar.php") ?>


        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">
              <?php $this->load->view("admin/_partials/navbar.php") ?>


							<!-- Begin Page Content -->
							                <div class="container-fluid">

							                    <!-- Page Heading -->
							                    <h1 class="h3 mb-2 text-gray-800">Tambah Data User</h1>
							                    <p class="mb-4"><!--DataTables is a third party plugin that is used to generate the demo table below.
							                        For more information about DataTables, please visit the <a target="_blank"
							                            href="https://datatables.net">official DataTables documentation</a>.--></p>

							                    <!-- DataTales Example -->
                                                <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div></div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <!--<h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>-->
                            </div>
                            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                    Nama Anggota
                                    </div>
                                    <div class="col-sm-6">
                                    <input type="text" name="nama" class="form-control form-control-user" value="<?php echo $this->input->post('nama') ?>" placeholder="Input Nama Lengkap">
						            <?php echo form_error('nama') ?>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                    Email
                                    </div>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control form-control-user" name="email" value="<?php echo $this->input->post('email') ?>"
                                        placeholder="Input Email">
                                        <?php echo form_error('email') ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    No Handphone
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-user" name="phone"
                                            placeholder="Input No Handphone" value="<?php echo $this->input->post('phone') ?>">
                                            <?php echo form_error('phone') ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                   Username
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-user" name="username"
                                            placeholder="Input Username" value="<?php echo $this->input->post('username') ?>">
                                            <?php echo form_error('username') ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                   Password
                                    </div>
                                    <div class="col-md">
                                        <input type="password" class="form-control form-control-user" name="password"
                                            placeholder="Input password" value="<?php echo $this->input->post('password') ?>">
                                            <?php echo form_error('password') ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                   Level
                                    </div>
                                    <div class="col-sm-6">
                                    <select name="level" class="form-control form-control-user">
							<option value="">--Pilih Level--</option>
							<option value="1">Admin</option>
							<option value="2">Petugas</option>
						</select>
						<?php echo form_error('level') ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                         
                                    </div>
                                </div>
                               <button class="btn btn-primary">Tambah Anggota</button>&nbsp;
                               <a href="<?php echo site_url('admin/anggota')?>" class="btn btn-primary">
                                    Kembali
                                </a>
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
							                <!-- /.container-fluid -->

							            </div>
							            <!-- End of Main Content -->

                <?php $this->load->view("admin/_partials/footer.php") ?>
            </div>
            <!-- End of Main Content -->



        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <?php $this->load->view("admin/_partials/scrolltop.php") ?>
    <?php $this->load->view("admin/_partials/modal.php") ?>
    <?php $this->load->view("admin/_partials/js.php") ?>




</body>

</html>

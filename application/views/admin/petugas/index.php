<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>
<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
      <?php $this->load->view("admin/_partials/sidebar.php") ?>


        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">
              <?php $this->load->view("admin/_partials/navbar.php") ?>


							<!-- Begin Page Content -->
							<div id="delay-alert">
		<?php
			echo $this->session->flashdata('add_success');
			echo $this->session->flashdata('update_success');
			echo $this->session->flashdata('delete_success');
			echo $this->session->flashdata('reset_success');
		?>
	</div>

							                <div class="container-fluid">

							                    <!-- Page Heading -->
							                    <h1 class="h3 mb-2 text-gray-800">Daftar User</h1>
							                    <p class="mb-4"><!--DataTables is a third party plugin that is used to generate the demo table below.
							                        For more information about DataTables, please visit the <a target="_blank"
							                            href="https://datatables.net">official DataTables documentation</a>.--></p>

							                    <!-- DataTales Example -->
							                    <div class="card shadow mb-4">
							                        <div class="card-header py-3">
							                            <h6 class="m-0 font-weight-bold text-primary"><a href="petugas/tambah">Tambah Data User</a></h6>
							                        </div>
							                        <div class="card-body">
							                            <div class="table-responsive">
							                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
							                                    <thead>
							                                        <tr>
                                                                    <th>Nama</th>
                                                                    <th>Email</th>
                                                                    <th>No HP</th>
                                                                    <th>Level</th>
							                                        <th>Action</th>
							                                        </tr>
							                                    </thead>
							                                    <tfoot>
							                                        <tr>
                                                                    <th>Nama</th>
                                                                    <th>Email</th>
                                                                    <th>No HP</th>
                                                                    <th>Level</th>
							                                        <th>Action</th>
							                                        </tr>
							                                    </tfoot>
							                                    <tbody>
																<?php foreach ($petugas as $p): ?>
							                                        <tr>	
                                                                    <td><?php echo $p->nama ?></td>
                                                                    <td><?php echo $p->email ?>
                                                                    <td><?php echo $p->hp ?>
                                                                    <td>

                                                                    <?php
												if($p->level == 1)
												{
													echo 'Administrator';
												}
												else{
													echo 'Operator';
												}
											?>
							                                            <td align="center">
																		
<a href="<?php echo site_url('admin/petugas/edit/'.$p->id_user) ?>"><img src="<?php echo base_url('assets/img/edit.png')?>"></a>&nbsp;
<a onclick="deleteConfirm('<?php echo site_url('admin/petugas/delete/'.$p->id_user) ?>')" href="#!" ><img src="<?php echo base_url('assets/img/delete.png')?>"></a>&nbsp;
<a onclick="resetConfirm('<?php echo site_url('admin/petugas/resetPassword/'.$p->id_user) ?>')" kode="<?php echo $p->id_user ?>" pass="<?php echo $p->username ?>" href="#!"><img src="<?php echo base_url('assets/img/lock-icon.png')?>"></a>											
																		
																		</td>
							                                        </tr>

																											<?php endforeach ?>
							                                    </tbody>
							                                </table>
							                            </div>
							                        </div>
							                    </div>

							                </div>
							                <!-- /.container-fluid -->

							            </div>
							            <!-- End of Main Content -->
																			

                <?php $this->load->view("admin/_partials/footer.php") ?>
            </div>
            <!-- End of Main Content -->



        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <?php $this->load->view("admin/_partials/scrolltop.php") ?>
    <?php $this->load->view("admin/_partials/modal.php") ?>
    <?php $this->load->view("admin/_partials/js.php") ?>




</body>

</html>
</div>
<script>
function deleteConfirm(url){
	$('#delay-alert').delay(2000).fadeOut(2000);
	$('#btn-delete').attr('href', url);
	$('#deleteModal').modal();
}
</script>

<script>
function resetConfirm(url){
	$('#delay-alert').delay(2000).fadeOut(2000);
	var kode = $(this).attr('kode');
	var pass = $(this).attr('pass');
	$('#btn-reset').attr('href', url);
	$('#resetModal').modal();
}
</script>
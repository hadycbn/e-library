<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>
<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
      <?php $this->load->view("admin/_partials/sidebar.php") ?>


        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">
              <?php $this->load->view("admin/_partials/navbar.php") ?>


							<!-- Begin Page Content -->
							                <div class="container-fluid">

							                    <!-- Page Heading -->
							                    <h1 class="h3 mb-2 text-gray-800">Form Ganti Password</h1>
							                    <p class="mb-4"><!--DataTables is a third party plugin that is used to generate the demo table below.
							                        For more information about DataTables, please visit the <a target="_blank"
							                            href="https://datatables.net">official DataTables documentation</a>.--></p>

							                    <!-- DataTales Example -->
                                                <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div></div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                            <div class="alert-delay">
		<?php 
			echo $this->session->flashdata('success'); 
			echo $this->session->flashdata('gagal');
		?>
	</div>
                            </div>
                            <form action="" class="form-horizontal" method="POST">
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                    Password Lama
                                    </div>
                                    <div class="col-sm-6">
                                    <input type="password" class="form-control" name="old" value="<?php echo $this->input->post('old') ?>">
						            <input type="checkbox" class="form-checkbox"> Show password
                                    <?php echo form_error('old') ?>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                    Password Baru
                                    </div>
                                    <div class="col-sm-6">
                                    <input type="password" class="form-control" name="new" value="<?php echo $this->input->post('new') ?>">
						            <?php echo form_error('new') ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                   Konfirmasi Passowrd Baru
                                    </div>
                                    <div class="col-sm-6">
                                    <input type="password" class="form-control" name="conf" value="<?php echo $this->input->post('conf') ?>">
						            <?php echo form_error('conf') ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                         
                                    </div>
                                </div>
                               <button class="btn btn-primary">Ganti Password</button>&nbsp;
                               <a href="<?php echo site_url('admin/petugas')?>" class="btn btn-primary">
                                    Kembali
                                </a>
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
							                <!-- /.container-fluid -->

							            </div>
							            <!-- End of Main Content -->

                <?php $this->load->view("admin/_partials/footer.php") ?>
            </div>
            <!-- End of Main Content -->



        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <?php $this->load->view("admin/_partials/scrolltop.php") ?>
    <?php $this->load->view("admin/_partials/modal.php") ?>
    <?php $this->load->view("admin/_partials/js.php") ?>




</body>

</html>
<script>
	$(function(){
		$('.alert-delay').delay(2000).fadeOut(2000);
	});
</script>
<script type="text/javascript">
	$(document).ready(function(){		
		$('.form-checkbox').click(function(){
			if($(this).is(':checked')){
				$('.form-control').attr('type','text');
			}else{
				$('.form-control').attr('type','password');
			}
		});
	});
</script>
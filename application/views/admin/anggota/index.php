<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>
<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
      <?php $this->load->view("admin/_partials/sidebar.php") ?>


        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">
              <?php $this->load->view("admin/_partials/navbar.php") ?>


							<!-- Begin Page Content -->
							<div id="delay-alert">
		<?php
			echo $this->session->flashdata('add_success');
			echo $this->session->flashdata('update_success');
			echo $this->session->flashdata('delete_success');
		?>
	</div>

							                <div class="container-fluid">

							                    <!-- Page Heading -->
							                    <h1 class="h3 mb-2 text-gray-800">Daftar Anggota</h1>
							                    <p class="mb-4"><!--DataTables is a third party plugin that is used to generate the demo table below.
							                        For more information about DataTables, please visit the <a target="_blank"
							                            href="https://datatables.net">official DataTables documentation</a>.--></p>

							                    <!-- DataTales Example -->
							                    <div class="card shadow mb-4">
							                        <div class="card-header py-3">
							                            <h6 class="m-0 font-weight-bold text-primary"><a href="anggota/tambah">Tambah Data Anggota</a></h6>
							                        </div>
							                        <div class="card-body">
							                            <div class="table-responsive">
							                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
							                                    <thead>
							                                        <tr>
                                                                    <th>Foto</th>
									                                <th>ID Anggota</th>
                                                                    <th>Nama</th>
                                                                    <th>Email</th>
                                                                    <th>No HP</th>
                                                                    <th>Username</th>
                                                                    <th>Tgl Daftar</th>
                                                                    <th>Status</th>
							                                        <th>Action</th>
							                                        </tr>
							                                    </thead>
							                                    <tfoot>
							                                        <tr>
                                                                    <th>Foto</th>
									                                <th>ID Anggota</th>
                                                                    <th>Nama</th>
                                                                    <th>Email</th>
                                                                    <th>No HP</th>
                                                                    <th>Username</th>
                                                                    <th>Tgl Daftar</th>
                                                                    <th>Status</th>
							                                        <th>Action</th>
							                                        </tr>
							                                    </tfoot>
							                                    <tbody>
																<?php foreach ($anggota as $a): ?>
							                                        <tr>
																	<td>
																	<?php
												if($a->foto == "")
												{
													echo '<img src="'.base_url('assets/img/no_photo.png').'" alt="" width="110" height="130">';
												}
												else{
													echo '<img src="'.base_url('assets/img/anggota/'.$a->foto).'" alt="" width="110" height="130">';
												}
											?>
																	</td>
                                                                    <td><?php echo $a->id_anggota ?></td> 	
                                                                    <td><?php echo $a->nama ?></td>
                                                                    <td><?php echo $a->email ?>
                                                                    <td><?php echo $a->hp ?>
                                                                    <td><?php echo $a->username ?>
                                                                    <td><?php echo $a->tgl_daftar ?>
                                                                    <td><?php echo $a->status ?>
							                                            <td width="10%">
																		
<a href="<?php echo site_url('admin/anggota/edit/'.$a->id_anggota) ?>"><img src="<?php echo base_url('assets/img/edit.png')?>"></a>
<a onclick="deleteConfirm('<?php echo site_url('admin/anggota/delete/'.$a->id_anggota) ?>')" href="#!" class="btn btn-small text-danger"><img src="<?php echo base_url('assets/img/delete.png')?>"></a>
											
																		
																		</td>
							                                        </tr>

																											<?php endforeach ?>
							                                    </tbody>
							                                </table>
							                            </div>
							                        </div>
							                    </div>

							                </div>
							                <!-- /.container-fluid -->

							            </div>
							            <!-- End of Main Content -->
																			

                <?php $this->load->view("admin/_partials/footer.php") ?>
            </div>
            <!-- End of Main Content -->



        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <?php $this->load->view("admin/_partials/scrolltop.php") ?>
    <?php $this->load->view("admin/_partials/modal.php") ?>
    <?php $this->load->view("admin/_partials/js.php") ?>




</body>

</html>
</div>
<script>
function deleteConfirm(url){
	$('#btn-delete').attr('href', url);
	$('#deleteModal').modal();
}
</script>
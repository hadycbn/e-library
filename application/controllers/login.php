<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	var $table = "t_anggota";
	var $pk = "id_anggota";

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('m_crud');

		//cek login
		if ($this->session->userdata('islogin')==true)
			redirect('dashboard','refresh');
	}

	public function index()
	{
		$data['title'] = "";
		//set rule validate
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_error_delimiters('<div class="text-danger" data-animate="fadeInLeft">', '</div>');
		$this->form_validation->set_message('required', ' This field is required');

		if ($this->form_validation->run()==true)
		{
			$email  = $this->input->post('username');
			$password  = md5($this->input->post('password'));
			$status = "Y";
			//cek login
			$cek  = $this->m_crud->loginmember($this->table, $email, $password, $status);
			//echo $this->db->last_query(); exit;

			if($cek->num_rows() > 0)
			{
				//get data
				$get = $cek->row_array();

				$this->session->set_userdata('islogin', true);
				$this->session->set_userdata('username', $get['username']);
				//$this->session->set_userdata('level', $get['level']);
				$this->session->set_userdata('level', '1');
				$this->session->set_userdata('nama', $get['nama']);

				//insert statistik pengunjung
				$ip = $this->input->ip_address();
				$tgl=date("Y-m-d");
				$record = array(
					'id_anggota' =>   $get['id_anggota'],
					'tanggal' => $tgl,
					'ip' => $ip
				);
				$this->m_crud->insertData('t_statistik_pengunjung', $record);
				redirect('dashboard');
			}
			else
			{
				$this->session->set_flashdata('login_fail', '<div class="alert alert-danger">Username atau Password Anda Salah</div>');
				redirect('login','refresh');
			}
		}

		//$this->template->display('login', $data);
		$this->load->view("login", $data);
	}

}

/* End of file  */
/* Location: ./application/controllers/ */

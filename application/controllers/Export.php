<?php

class Export extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('Kategori_model');
                $this->load->helper('url');
	}

	function index(){
		$data['user'] = $this->Kategori_model->tampil_data()->result();
		$this->load->view('export',$data);
	}
}

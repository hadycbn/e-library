<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kategori extends CI_Controller {
  var $table = "t_kategori";
  var $pk    = "kode";

  public function __construct()
	{
		parent::__construct();
		$this->load->library(array('pagination','form_validation', 'upload'));
		$this->load->model('m_crud');
		$this->cekLogin();
	}

	public function index()
	{
		//$data['title'] = "Data Buku";
		//$limit = 10000;
		//$this->db->order_by('kode', 'desc');
		$data['kategori'] = $this->m_crud->no_paging($this->table)->result();
		//get data
		//$this->template->display('admin/buku/index', $data);
		$this->load->view("kategori/index", $data);

	}

  public function tambah()
	{
		$data['title'] = 'Tambah Kategori';
		$this->cekValidasi();

		if ($this->form_validation->run()==true)
		{
			$record = array(
							  'kode' => '',
							  'kategori' => $this->input->post('nama'),
								'status' => $this->input->post('status')
						   );

			$this->m_crud->insertData($this->table, $record);
			$this->session->set_flashdata('add_success', '<div class="alert alert-success">Data sukses ditambahkan</div>');
			redirect('kategori','refresh');
		}

		$this->template->display('kategori/tambah', $data);
	}

  public function hapus()
	{
		$id = $this->input->post('id_hapus');
		$this->session->set_flashdata('delete_success', '<div class="alert alert-danger">Data terhapus</div>');
		$this->m_crud->deleteData($this->table, $this->pk, $id);
	}

  public function cariData()
	{
		$data['title'] = "Hasil Pencarian kategori dengan kata kunci ".$this->input->post('cari');
		//set value
		$input = $this->input->post('cari');
		$this->db->like('kategori', $input);
		//$this->db->or_like('judul', $input);
		$query = $this->db->get($this->table);
		//echo $this->db->last_query(); exit;
			$cek_data = $query->num_rows();

		if ($cek_data > 0)
		{
			$data['kategori'] = $query->result();
			$this->template->display('kategori/cari', $data);
			$this->session->set_flashdata('filter_success', '<div class="alert alert-success">Pencarian Sukses</div>');
		}
		else {
			$data['message'] = '<div class="alert alert-danger">OOPs ... Data yang anda Cari Tidak ada</div>';
			$this->template->display('kategori/tidakada', $data);
		}

	}

  public function cekValidasi()
	{
		$this->form_validation->set_rules('nama', 'Nama Ketegori', 'required');
		$this->form_validation->set_rules('status', 'Status Kategori', 'required');
		//$this->form_validation->set_rules('foto', 'File E-book', 'required');
		$this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');
	}

  public function cekLogin()
	{
		if ($this->session->userdata('islogin')==false)
			redirect('login','refresh');
	}
}

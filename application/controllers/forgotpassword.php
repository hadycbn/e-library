<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forgotpassword extends CI_Controller {
  var $table = "t_anggota";
  var $pk    = "id_anggota";

  public function __construct()
	{
		parent::__construct();
    $this->load->library(array('form_validation'));
		$this->load->model('m_crud');
	}

  public function index()
  {
    $data['title'] = "";
    //set rule validate
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_check_email_not_exists');
		$this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');

    if ($this->form_validation->run()==true)
		{
      $email  = $this->input->post('email');
      $kode  = md5($this->input->post('email'));
      $cek  = $this->m_crud->idanggota($this->table, $email);
      $get = $cek->row_array();
      //$this->session->set_userdata('nama', $get['nama']);
      $record = array(
        'id_anggota' => $get['id_anggota'],
        'kode' =>  $kode,
        'tgl_input' => date("Y-m-d")
        //'foto' => $foto
						   );

  		$this->m_crud->insertData('t_anggota_verification', $record);
  			redirect('forgotpassword/konfirmasireset','refresh');
  		}

      //$this->template->display('forgotpassword', $data);
      $this->load->view("forgotpassword", $data);
      }

      public function konfirmasireset()
      {
        $data['title'] = "";
        $this->template->display('konfirmasireset', $data);
      }

      public function creatnewpassword()
      {
        $data['title'] = "";
        //set rule validate
    		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|matches[konfirmasi]');
        $this->form_validation->set_rules('konfirmasi', 'Confirm Password', 'required|min_length[6]');
    		$this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');

        if ($this->form_validation->run()==true)
    		{
          $email  = $this->input->post('email');
          $kode  = md5($this->input->post('email'));
          $cek  = $this->m_crud->idanggota($this->table, $email);
          $get = $cek->row_array();
          //$this->session->set_userdata('nama', $get['nama']);
          $record = array(
            'id_anggota' => $get['id_anggota'],
            'kode' =>  $kode,
            'tgl_input' => date("Y-m-d")
            //'foto' => $foto
    						   );

      		$this->m_crud->insertData('t_anggota_verification', $record);
      			redirect('forgotpassword/konfirmasireset','refresh');
      		}

          $this->template->display('resetpassword', $data);
          }



    // Check if email does not exists
    public function check_email_not_exists($email){
    $this->form_validation->set_message('check_email_not_exists', 'Email not registered in our systems!');
    if($this->m_crud->check_email_not_exists($email)){
    return true;
    } else {
    return false;
    }
    }
}

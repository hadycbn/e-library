<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Buku extends CI_Controller {

	var $table = "t_buku";
	var $pk    = "kd_buku";
	var $cat= "status";

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('pagination','form_validation', 'upload'));
		$this->load->model('m_crud');
		$this->cekLogin();
	}

	public function index($offset=null)
	{
		$data['title'] = "Data Buku";
		//$limit = 10000;
		$this->db->order_by('kd_buku', 'desc');
		$data['buku'] = $this->m_crud->listbuku($this->db)->result();
		$this->load->view("admin/buku/index", $data);

	}


	public function tambah()
	{
		$data['title'] = "Tambah Buku";
		$lokasi="./assets/buku/".$this->input->post('kategori');
		$this->cekValidasi();

		if ($this->form_validation->run()==true)
		{
			//upload
				$config['upload_path'] = $lokasi;
				$config['allowed_types'] = 'jpeg|jpg|png|pdf';
				$config['max_size']  = '';
				$config['max_width']  = '';
				$config['max_height']  = '';
				$config['encrypt_name'] = TRUE;
				//$new_name = time().$_FILES["foto"]['name'];

				$this->upload->initialize($config);
				if (! $this->upload->do_upload('cover')){
					$this->upload->do_upload('ebook');
					$file1 = $this->upload->data();
					$filename=$file1['file_name'];
					$cover = "";

					$record = array(
							 'kd_buku' => $this->input->post('kd_buku'),
							 'judul' => $this->input->post('judul'),
							 'penerbit' => $this->input->post('penerbit'),
							 'pengarang' => $this->input->post('pengarang'),
							 'deskripsi' => $this->input->post('deskripsi'),
							 'kategori' => $this->input->post('kategori'),
							 'file_ebook' => $filename,
							 //'cover' => $cover,
							 'tgl_masuk' => date('Y-m-d'),
							 'berbayar' => $this->input->post('berbayar'),
							 'status' => 'Y'
						   );
				}else{
					$this->upload->initialize($config);
					$this->upload->do_upload('ebook');
					$file1 = $this->upload->data();
					$filename=$file1['file_name'];

				 	$this->upload->do_upload('cover');
				 	$file2 = $this->upload->data();
				 	$cover=$file2['file_name'];

					 $record = array(
							 'kd_buku' => $this->input->post('kd_buku'),
							 'judul' => $this->input->post('judul'),
							 'penerbit' => $this->input->post('penerbit'),
							 'pengarang' => $this->input->post('pengarang'),
							 'deskripsi' => $this->input->post('deskripsi'),
							 'kategori' => $this->input->post('kategori'),
							 'file_ebook' => $filename,
							 'cover' => $cover,
							 'tgl_masuk' => date('Y-m-d'),
							 'berbayar' => $this->input->post('berbayar'),
							 'status' => 'Y'
						   );
				   }

			$this->m_crud->insertData($this->table, $record);
		//
			$this->session->set_flashdata('add_success', '<div class="alert alert-success" data-animate=""> Input buku baru sukses.</div>');
			redirect('admin/buku');
		}

		$data['autonumber'] = $this->m_crud->autoNumber($this->table, $this->pk, 2, 'BUK');
		$data['user'] = $this->m_crud->kategori();
		$this->load->view("admin/buku/tambah", $data);
		//print_r($this->db->last_query());
		//print_r($data);
	}

	public function edit()
	{
		$data['title'] = "Edit Buku";
		$id = $this->uri->segment('4');
		$lokasi="./assets/buku/".$this->input->post('kategori');


		//get id
		$this->db->where( 't_buku.kd_buku', $id);
		$get_id = $this->m_crud->listbuku($this->db);

		//$data['buku'] = $this->m_crud->listbuku($this->db)->result();

		$this->editValidasi();
		if ($this->form_validation->run()==true)
		{
				$record = array(
								 'judul' => $this->input->post('judul'),
								 'penerbit' => $this->input->post('penerbit'),
								 'pengarang' => $this->input->post('pengarang'),
								 'deskripsi' => $this->input->post('deskripsi'),
								 'berbayar' => $this->input->post('berbayar'),
								 'status' => $this->input->post('status')
							   );


				$this->m_crud->updateData($this->table, $record, $this->pk, $id);
				$this->session->set_flashdata('update_success', '<div class="alert alert-warning"> Data Berhasil di update</div>');
				redirect('admin/buku');
		}

			$data['buku'] = $get_id->result();
			$data['user'] = $this->m_crud->kategori();
			//echo $this->db->last_query(); exit;
			//$this->template->display('buku/edit', $data);
			$this->load->view("admin/buku/edit", $data);
	}

	public function editebook()
	{
		$data['title'] = "Edit Buku";
		$id = $this->uri->segment('4');
		$lokasi="./assets/buku/".$this->input->post('kategori');

		//get id
		$this->db->where( 't_buku.kd_buku', $id);
		$get_id = $this->m_crud->listbuku($this->db);

		$this->editValidasi();
		if ($this->form_validation->run()==true)
		{
			//upload
				$config['upload_path'] = $lokasi;
				$config['allowed_types'] = 'jpeg|jpg|png|pdf';
				$config['max_size']  = '';
				$config['max_width']  = '';
				$config['max_height']  = '';
				$config['encrypt_name'] = TRUE;

				$this->upload->initialize($config);
				if (! $this->upload->do_upload('cover')){
					$this->upload->do_upload('ebook');
					$file1 = $this->upload->data();
					$filename=$file1['file_name'];
					$cover = "";

					$record = array(
						'file_ebook' => $filename,
						'tgl_masuk' => date('Y-m-d')
					  );
				}elseif (! $this->upload->do_upload('ebook')){
					$this->upload->do_upload('cover');
					$file2 = $this->upload->data();
					$cover=$file2['file_name'];

					$record = array(
						'cover' => $cover,
						'tgl_masuk' => date('Y-m-d')
					  );
				}
				else{
					$this->upload->initialize($config);
					$this->upload->do_upload('ebook');
					$file1 = $this->upload->data();
					$filename=$file1['file_name'];

				 	$this->upload->do_upload('cover');
				 	$file2 = $this->upload->data();
				 	$cover=$file2['file_name'];

					 $record = array(
						'file_ebook' => $filename,
						'cover' => $cover,
						'tgl_masuk' => date('Y-m-d')
					  );
				}



				//$ebook_id = $get_id->row_array();
				//unlink("assets/img/buku/".$ebook_id['cover']);


				$this->m_crud->updateData($this->table, $record, $this->pk, $id);
				$this->session->set_flashdata('update_success', '<div class="alert alert-warning"> Data Berhasil di update</div>');
				redirect('admin/buku');
		}

			$data['buku'] = $get_id->result();
			$data['user'] = $this->m_crud->kategori();
			$this->load->view("admin/buku/editebook", $data);
	}

	public function hapus()
	{
		$id_hapus = $this->input->post('id_hapus');
		$this->session->set_flashdata('delete_success', '<div class="alert alert-danger">Delete Data Success</div>');
		$this->m_crud->deleteData($this->table, $this->pk, $id_hapus);
		//echo $this->db->last_query(); exit;
	}

	public function delete($id=null)
    {
        if (!isset($id)) show_404();

        if ($this->m_crud->delete($this->table, $this->pk, $id)) {
			$this->session->set_flashdata('delete_success', '<div class="alert alert-danger">Delete Data Success</div>');
            redirect(site_url('admin/buku'));
        }
    }

	public function cariData()
	{
		$data['title'] = "Hasil Pencarian buku dengan kata kunci ".$this->input->post('cari');
		//set value
		$input = $this->input->post('cari');
		$this->db->like('kd_buku', $input);
		$this->db->or_like('judul', $input);
		$query = $this->db->get($this->table);
		//echo $this->db->last_query(); exit;
			$cek_data = $query->num_rows();

		if ($cek_data > 0)
		{
			$data['buku'] = $query->result();
			$this->template->display('buku/cari', $data);
			$this->session->set_flashdata('filter_success', '<div class="alert alert-success">Pencarian Sukses</div>');
		}
		else {
			$data['message'] = '<div class="alert alert-danger">OOPs ... Data yang anda Cari Tidak ada</div>';
			$this->template->display('buku/tidakada', $data);
		}

	}

	public function cekValidasi()
	{
		$this->form_validation->set_rules('judul', 'Judul', 'required');
		$this->form_validation->set_rules('kategori', 'Kategori', 'required');
		//$this->form_validation->set_rules('penerbit', 'Penerbit', 'required');
		//$this->form_validation->set_rules('pengarang', 'Pengarang', 'required');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
		if (empty($_FILES['ebook']['name']))
			{
				$this->form_validation->set_rules('ebook', 'Ebook', 'required');
			}
		$this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');
	}
	public function editValidasi()
	{
		$this->form_validation->set_rules('kd_buku', 'Kode Buku', 'required');
		$this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');
	}
	public function cekLogin()
	{
		if ($this->session->userdata('islogin')==false)
			redirect('admin/login','refresh');
	}



}

/* End of file  */
/* Location: ./application/controllers/ */

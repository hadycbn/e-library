<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Akses extends CI_Controller {

	var $table = "t_buku_berbayar";
	var $pk    = "id";
	//var $cat= "status";

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('pagination','form_validation', 'upload'));
		$this->load->model('m_crud');
		$this->cekLogin();
	}

	public function index($offset=null)
	{
		$data['title'] = "Data Buku";
		$data['akses'] = $this->m_crud->requestakses($this->db)->result();
		//print_r($this->db->last_query());
		$this->load->view("admin/akses/index", $data);

	}

  public function tambah()
	{
		$data['title'] = 'Tambah Kategori';
		$this->cekValidasi();

		if ($this->form_validation->run()==true)
		{
			$record = array(
							  'kode' => '',
							  'kategori' => $this->input->post('nama'),
								'status' => $this->input->post('status')
						   );

			$this->m_crud->insertData($this->table, $record);
			$this->session->set_flashdata('add_success', '<div class="alert alert-success">Data sukses ditambahkan</div>');
			redirect('admin/kategori','refresh');
		}

		$this->load->view("admin/kategori/tambah", $data);
	}

	public function edit()
	{
		$data['title'] = "Edit Buku";
		$aid = $this->uri->segment('4');
		

		//get id
		$akses_id = $this->m_crud->get_request_id($this->db, $aid);

		$this->cekValidasi();
		if ($this->form_validation->run()==true)
		{
				$record = array(
								 'status' => $this->input->post('status')
							   );
			

				$this->m_crud->updateData($this->table, $record, $this->pk, $aid);
				$this->session->set_flashdata('update_success', '<div class="alert alert-warning"> Data Berhasil di update</div>');
				redirect('admin/akses');
		}

			$data['akses'] = $akses_id->result();
			//echo $this->db->last_query(); exit;
			//$this->template->display('buku/edit', $data);
			$this->load->view("admin/akses/edit", $data);
	}

	public function delete($id=null)
    {
        if (!isset($id)) show_404();

        if ($this->m_crud->delete($this->table, $this->pk, $id)) {
			$this->session->set_flashdata('delete_success', '<div class="alert alert-danger">Delete Data Success</div>');
            redirect(site_url('admin/akses'));
        }
    }


  public function cekValidasi()
	{
		$this->form_validation->set_rules('nama', 'Nama Ketegori', 'required|callback_check_kategori_exists');
		$this->form_validation->set_rules('status', 'Status Kategori', 'required');
		//$this->form_validation->set_rules('nama', 'File E-book', 'required');
		$this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');
	}

  public function cekLogin()
	{
		if ($this->session->userdata('islogin')==false)
			redirect('admin/login','refresh');
	}

	// Check if username exists
    public function check_kategori_exists($kategori){
		$this->form_validation->set_message('check_kategori_exists', 'Category name already exists, please revise');
		if($this->m_crud->check_kategori_exists($kategori)){
		return true;
		} else {
		return false;
		}
		}
}

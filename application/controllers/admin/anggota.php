<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anggota extends CI_Controller {

	var $table = "t_anggota";
	var $pk = "id_anggota";

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation', 'upload', 'pagination'));
		$this->load->model('m_crud');
		$this->cekLogin();
	}

	public function index($offset=null)
	{
		$data['title'] = "Data Buku";
		//$limit = 10000;
		$this->db->order_by('id_anggota', 'desc');
		$data['anggota'] = $this->m_crud->no_paging($this->table)->result();
		//get data
		//$this->template->display('admin/buku/index', $data);
		$this->load->view("admin/anggota/index", $data);

	}

	public function tambah()
	{
		$data['title'] = "Tambah Anggota";
		$this->cekValidasi();

		if ($this->form_validation->run()==true)
		{
			//upload
			$config['upload_path'] = './assets/img/anggota/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']  = '';
				$config['max_width']  = '';
				$config['max_height']  = '';

				$this->upload->initialize($config);
				if ( ! $this->upload->do_upload('foto')){
					$foto = "";
				}
				else{
					$foto = $_FILES['foto']['name'];
				}

			$record = array(
	        'id_anggota' => $this->input->post('id_anggota'),
	        'nama' => $this->input->post('nama'),
	        'email' => $this->input->post('email'),
	        'hp' => $this->input->post('phone'),
	        'username' => $this->input->post('email'),
	        'password' => md5($this->input->post('password')),
			'foto' => $foto,
	        'tgl_daftar' => date("Y-m-d"),
	        'status' => 'Y'
							   );

				$this->m_crud->insertData($this->table, $record);
				$this->session->set_flashdata('add_success', '<div class="alert alert-success">Data anggota baru berhasil di input</div>');
				redirect('admin/anggota','refresh');
		}

		$data['autonumber'] = $this->m_crud->autoNumber($this->table, $this->pk, 3, date("Ymd"));
		$this->load->view("admin/anggota/tambah", $data);
	}

	public function edit()
	{
		$data['title'] = "Edit Anggota";
		$id = $this->uri->segment(4);
		$query = $this->m_crud->get_id($this->table, $this->pk, $id);

		$this->editValidasi();
		if ($this->form_validation->run()==true)
		{
			//upload
			$config['upload_path'] = './assets/img/anggota/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '';
			$config['max_width']  = '';
			$config['max_height']  = '';

			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('foto')){
				$record = array(
								 'nama' => $this->input->post('nama'),
								 'email' => $this->input->post('email'),
								 'hp' => $this->input->post('phone'),
								 'status' => $this->input->post('status')
					       	   );
			}
			else{
				$foto = $_FILES['foto']['name'];
				$record = array(
								'nama' => $this->input->post('nama'),
								'email' => $this->input->post('email'),
								'hp' => $this->input->post('phone'),
								'foto' => $foto,
								'status' => $this->input->post('status')
								);

				$unlink_id = $query->row_array();
				unlink('./assets/img/anggota/'.$unlink_id['foto']);
			}

			$this->m_crud->updateData($this->table, $record, $this->pk, $id);
			$this->session->set_flashdata('update_success','<div class="alert alert-danger"> Update anggota berhasil</div>');
			redirect('admin/anggota');
		}

		$data['anggota'] = $query->result();
		$this->load->view("admin/anggota/edit", $data);

	}

	public function hapus()
	{
		$id = $this->input->post('id_hapus');
		$detail = $this->m_crud->get_id($this->table, $this->pk, $id)->row_array();
		unlink("assets/img/anggota/".$detail['foto']);

		$this->m_crud->deleteData($this->table, $this->pk, $id);
		$this->session->set_flashdata('delete_success', '<div class="alert alert-danger">Data berhasil dihapus</div>');
	}

	public function delete($id=null)
    {
        if (!isset($id)) show_404();

        if ($this->m_crud->delete($this->table, $this->pk, $id)) {
			$this->session->set_flashdata('delete_success', '<div class="alert alert-danger">Delete Data Success</div>');
            redirect(site_url('admin/anggota'));
        }
    }

	public function cekValidasi()
	{
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_check_email_exists');
		$this->form_validation->set_rules('phone', 'phone', 'required|numeric|callback_check_phone_exists');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
		/*if (empty($_FILES['foto']['name']))
			{
				$this->form_validation->set_rules('foto', 'Foto', 'required');
			}*/
		$this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');
	}

	public function editValidasi()
	{
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('phone', 'phone', 'required|numeric');
    	//$this->form_validation->set_rules('foto', 'Foto','required');
		//$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
		$this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');
	}

			// Check if username exists
		public function check_username_exists($username){
			$this->form_validation->set_message('check_username_exists', 'Username already exists, please revise');
		 if($this->m_crud->check_username_exists($username)){
		 return true;
		 } else {
		 return false;
		 }
		}
				// Check if username exists
		public function check_phone_exists($phone){
		$this->form_validation->set_message('check_phone_exists', 'Phone number already exists, please revise');
		if($this->m_crud->check_phone_exists($phone)){
		return true;
		} else {
		return false;
		}
		}
		// Check if username exists
		public function check_email_exists($email){
		$this->form_validation->set_message('check_email_exists', 'Email already exists, please revise');
		if($this->m_crud->check_email_exists($email)){
		return true;
		} else {
		return false;
		}
		}

	public function cariData()
	{
		$data['title'] = "Hasil Pencarian Data Anggota";
		//set value
		$input = $this->input->post('cari');
		$this->db->like('nim', $input);
		$this->db->or_like('nama', $input);
		$query = $this->db->get($this->table);
		//echo $this->db->last_query(); exit;
			$cek_data = $query->num_rows();

		if ($cek_data > 0)
		{
			$data['anggota'] = $query->result();
			$this->template->display('anggota/cari', $data);
			$this->session->set_flashdata('filter_success', '<div class="alert alert-success">Pencarian Sukses</div>');
		}
		else {
			$data['message'] = '<div class="alert alert-danger">OOPs ... Data yang anda Cari Tidak ada</div>';
			$this->template->display('anggota/tidakada', $data);
		}

	}

	public function cekLogin()
	{
		if ($this->session->userdata('islogin') == false)
			redirect('admin/login','refresh');
	}
}

/* End of file anggota.php */
/* Location: ./application/controllers/anggota.php */

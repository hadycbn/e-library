<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 require 'vendor/autoload.php';
	use PhpOffice\PhpSpreadsheet\Spreadsheet;
	use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Lap_anggota extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_laporan');
		//cek login
		if ($this->session->userdata('islogin') == false)
			redirect('admin/login','refresh');
	}

	public function index()
	{
		$data['title'] = "Laporan Anggota";
		$this->load->view("admin/laporan/lapanggota", $data);
		//$this->template->display('admin/laporan/lapanggota', $data);
	}

	public function get_report()
	{

		$tgl1 = $this->input->post('tgl1');
		$tgl2 = $this->input->post('tgl2');
		$this->session->set_flashdata('a', $tgl1);
		$this->session->set_flashdata('b', $tgl2);
	
		$cek = $this->m_laporan->get_anggota($tgl1, $tgl2);
		//echo $this->db->last_query(); exit;
		//cek datanyal 
		if ($cek->num_rows == 0) 
		{
			echo "";
		}
		else
		{
			$data['tampil_data'] = $cek->result();
			$this->load->view('admin/laporan/tampil_anggota', $data);
		}
		
	}
		public function excel()
		{
			$this->load->model('peminjaman_model');
			$spreadsheet = new Spreadsheet();
			$sheet = $spreadsheet->getActiveSheet();
      		$sheet->setCellValue('A1', 'No');
			$sheet->setCellValue('B1', 'ID Anggota');
			$sheet->setCellValue('C1', 'Nama Anggota');
			$sheet->setCellValue('D1', 'Email');
			$sheet->setCellValue('E1', 'No Handphone');
			$sheet->setCellValue('F1', 'Tanggal Daftar');
      		$sheet->setCellValue('G1', 'Status');
			
			  //param
			
			//$cek = $this->m_laporan->get_anggota($tgl1, $tgl2);
			$satu = $this->session->flashdata('a');
			$dua = $this->session->flashdata('b');
			$anggota = $this->peminjaman_model->getAll($satu, $dua);
			//$data = $anggota->result();
			//echo $this->db->last_query(); exit;
			$no = 1;
			$x = 2;
			foreach($anggota as $row)
			{
        if ($row->status == "Y") {
          $status="Aktif";
        }
        elseif ($row->status == "N") {
          $status="Tidak Aktif";
        }
      	$sheet->setCellValue('A'.$x, $no++);
		$sheet->setCellValue('B'.$x, $row->id_anggota);
		$sheet->setCellValue('C'.$x, $row->nama);
		$sheet->setCellValue('D'.$x, $row->email);
        $sheet->setCellValue('E'.$x, $row->hp);
        $sheet->setCellValue('F'.$x, $row->tgl_daftar);
		$sheet->setCellValue('G'.$x, $status);
		$x++;
			}
      //$tgl=date("Ymd_hms");
      $tgl=date("Ymd");
      $writer = new Xlsx($spreadsheet);
			$filename = 'laporan_anggota_'.$tgl;

			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"');
			header('Cache-Control: max-age=0');

			$writer->save('php://output');
		}

}

/* End of file lap_anggota.php */
/* Location: ./application/controllers/lap_anggota.php */
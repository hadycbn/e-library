<?php

class Overview extends CI_Controller {

    public function __construct()
    {
		parent::__construct();
		$this->cekLogin();
		$this->load->model('statistik_buku');
		$this->load->model('statistik_pengunjung');
		$this->load->model('statistik_anggota');
	}

	public function index()
	{
		// statistik buku
		$data['statistik_buku'] = [
			'jumlah_buku_gratis' => $this->statistik_buku->count_buku_gratis(),
			'jumlah_buku_berbayar' => $this->statistik_buku->count_buku_berbayar(),
			'jumlah_buku_dibaca' => $this->statistik_buku->count_buku_dibaca(),
			'jumlah_buku_didownload' => $this->statistik_buku->count_buku_didownload(),
			'jumlah_total_buku' => $this->statistik_buku->count_total_buku(),
			'top_5_buku_paling_banyak_dibaca' => $this->statistik_buku->top_buku_paling_banyak_dibaca(),
		];

		// statistik pengunjung
		$data['statistik_pengunjung'] = [
			'jumlah_pengunjung_online' => $this->statistik_pengunjung->count_pengunjung_online(),
			'jumlah_pengunjung_hari_ini' => $this->statistik_pengunjung->count_pengunjung_hari_ini(),
			'jumlah_pengunjung_bulan_ini' => $this->statistik_pengunjung->count_pengunjung_bulan_ini(),
			'jumlah_total_pengunjung' => $this->statistik_pengunjung->count_total_pengunjung(),
			'grafik_pengunjung' => $this->statistik_pengunjung->grafik_pengunjung_tahun_ini(),
		];

		// statistik anggota
		$data['statistik_anggota'] = [
			'jumlah_anggota_hari_ini' => $this->statistik_anggota->count_anggota_hari_ini(),
			'jumlah_anggota_minggu_ini' => $this->statistik_anggota->count_anggota_minggu_ini(),
			'jumlah_anggota_bulan_ini' => $this->statistik_anggota->count_anggota_bulan_ini(),
			'jumlah_total_anggota' => $this->statistik_anggota->count_total_anggota(),
			'grafik_anggota' => $this->statistik_anggota->grafik_anggota_tahun_ini(),
		];

        // load view admin/overview.php
        $this->load->view("admin/overview", $data);
	}

	public function cekLogin()
	{
		if ($this->session->userdata('islogin')==false)
			redirect('admin/login','refresh');
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('admin/login','refresh');
	}
	public function gantiPassword()
	{
		$data['title'] = "Ganti Password";
		$this->load->library('form_validation');
		$this->form_validation->set_rules('old', 'Old Password', 'required');
		$this->form_validation->set_rules('new', 'New Password', 'required|matches[conf]');
		$this->form_validation->set_rules('conf', 'Password Confirmation', 'required');
		$this->form_validation->set_error_delimiters('<div class="text-danger" data-animate="fadeInLeft">','</div>');

		if ($this->form_validation->run()==true)
		{
			$this->load->model('m_crud');
			$cek = $this->m_crud->cekPassword('t_user', $this->session->userdata('username'));

			if ($cek->num_rows() > 0)
			{
				$old = md5($this->input->post('old'));
				$pass = $cek->row_array();

				if ($old == $pass['password'])
				{
					$this->db->where( 'id_user', $pass['id_user']);
					$this->db->update('t_user', array('password'=>md5($this->input->post('new'))));
					$this->session->set_flashdata('success', '<div class="alert alert-success">Password berhasil di update</div>');
					redirect('dashboard/gantiPassword','refresh');
				}
				else
				{
					$this->session->set_flashdata('gagal', '<div class="alert alert-danger">Password lama anda salah. Coba kembali..</div>');
					redirect('dashboard/gantiPassword');
				}

			}
		}

		$this->template->display('ganti_password', $data);
	}
}
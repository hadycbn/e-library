<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kategori extends CI_Controller {
  var $table = "t_kategori";
  var $pk    = "kode";

  public function __construct()
	{
		parent::__construct();
		$this->load->library(array('pagination','form_validation', 'upload'));
		$this->load->model('m_crud');
		$this->cekLogin();
	}

	public function index()
	{
		//$data['title'] = "Data Buku";
		//$limit = 10000;
		//$this->db->order_by('kode', 'desc');
		$data['kategori'] = $this->m_crud->no_paging($this->table)->result();
		//get data
		//$this->template->display('admin/buku/index', $data);
		$this->load->view("admin/kategori/index", $data);

	}

	public function list()
	{
		$data['title'] = "Data Buku";
		$this->load->view("admin/kategori/list", $data);

	}

  public function tambah()
	{
		$data['title'] = 'Tambah Kategori';
		$this->cekValidasi();

		if ($this->form_validation->run()==true)
		{
			$record = array(
							  'kode' => '',
							  'kategori' => $this->input->post('nama'),
							  'template' => $this->input->post('template'),
								'status' => $this->input->post('status')
						   );

			$this->m_crud->insertData($this->table, $record);
			$this->session->set_flashdata('add_success', '<div class="alert alert-success">Data sukses ditambahkan</div>');
			redirect('admin/kategori','refresh');
		}
		$data['user'] = $this->m_crud->kategori();
		$this->load->view("admin/kategori/tambah", $data);
	}

  
	public function delete($id=null)
    {
        if (!isset($id)) show_404();

        if ($this->m_crud->delete($this->table, $this->pk, $id)) {
			$this->session->set_flashdata('delete_success', '<div class="alert alert-danger">Delete Data Success</div>');
            redirect(site_url('admin/kategori'));
        }
    }


  public function cekValidasi()
	{
		$this->form_validation->set_rules('nama', 'Nama Ketegori', 'required|callback_check_kategori_exists');
		$this->form_validation->set_rules('status', 'Status Kategori', 'required');
		//$this->form_validation->set_rules('nama', 'File E-book', 'required');
		$this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');
	}

  public function cekLogin()
	{
		if ($this->session->userdata('islogin')==false)
			redirect('admin/login','refresh');
	}

	// Check if username exists
    public function check_kategori_exists($kategori){
		$this->form_validation->set_message('check_kategori_exists', 'Category name already exists, please revise');
		if($this->m_crud->check_kategori_exists($kategori)){
		return true;
		} else {
		return false;
		}
		}
}

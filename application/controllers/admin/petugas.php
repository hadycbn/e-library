<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Petugas extends CI_Controller {

	var $table = 't_user';
	var $pk = 'id_user';

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('m_crud');
		$this->cekLogin();
	}

	public function index($offset=null)
	{
		$data['title'] = "Data Buku";
		//$limit = 10000;
		$this->db->order_by('id_user', 'desc');
		$data['petugas'] = $this->m_crud->no_paging($this->table)->result();
		//get data
		//$this->template->display('admin/buku/index', $data);
		$this->load->view("admin/petugas/index", $data);

	}

	public function tambah()
	{
		$data['title'] = 'Tambah Petugas';
		$this->cekValidasi();

		if ($this->form_validation->run()==true)
		{
			$record = array(
							  'id_user' => '',
							  'nama' => $this->input->post('nama'),
								'email' => $this->input->post('email'),
								'hp' => $this->input->post('phone'),
							  'username' => $this->input->post('username'),
							  'password' => md5($this->input->post('password')),
							  'level' => $this->input->post('level')
						   );

			$this->m_crud->insertData($this->table, $record);
			$this->session->set_flashdata('add_success', '<div class="alert alert-success">Data sukses ditambahkan</div>');
			redirect('admin/petugas','refresh');
		}
		$this->load->view("admin/petugas/tambah", $data);
	}

	public function edit()
	{
		$data['title'] = "Edit Petugas";
		$id = $this->uri->segment(4);
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('email', 'Email', 'valid_email');
		$this->form_validation->set_rules('phone', 'phone', 'required');
		$this->form_validation->set_rules('phone', 'phone', 'numeric');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('level', 'Level', 'required');
		$this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');

		if ($this->form_validation->run() == true)
		{
			$record = array(
					'nama' => $this->input->post('nama'),
					'email' => $this->input->post('email'),
					'hp' => $this->input->post('phone'),
					'username' => $this->input->post('username'),
					'level' => $this->input->post('level')
				);

			$this->session->set_flashdata('update_success', '<div class="alert alert-info">Data berhasil di update</div>');
			$this->m_crud->updateData($this->table, $record, $this->pk, $id);
			redirect('admin/petugas','refresh');
		}

		$data['petugas'] = $this->m_crud->get_id($this->table, $this->pk, $id)->result();
		$this->load->view("admin/petugas/edit", $data);
	}

	
	public function delete($id=null)
    {
        if (!isset($id)) show_404();

        if ($this->m_crud->delete($this->table, $this->pk, $id)) {
			$this->session->set_flashdata('delete_success', '<div class="alert alert-danger">Delete Data Success</div>');
            redirect(site_url('admin/petugas'));
        }
    }

	public function cariData()
	{
		$data['title'] = "Hasil Pencarian Data Petugas";
		//set value
		$input = $this->input->post('cari');
		$this->db->like('nama', $input);
		$this->db->or_like('username', $input);
		$query = $this->db->get($this->table);
		//echo $this->db->last_query(); exit;
			$cek_data = $query->num_rows();

		if ($cek_data > 0)
		{
			$data['petugas'] = $query->result();
			$this->template->display('petugas/cari', $data);
			$this->session->set_flashdata('filter_success', '<div class="alert alert-success">Pencarian Sukses</div>');
		}
		else {
			$data['message'] = '<div class="alert alert-danger">OOPs ... Data yang anda Cari Tidak ada</div>';
			$this->template->display('petugas/tidakada', $data);
		}

	}


	public function cekValidasi()
	{
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_check_email_user_exists');
		$this->form_validation->set_rules('phone', 'phone', 'required|numeric');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
		$this->form_validation->set_rules('level', 'Level', 'required');
		$this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');
	}

				// Check if username exists
				public function check_email_user_exists($email){
					$this->form_validation->set_message('check_email_user_exists', 'Email already exists, please revise');
					if($this->m_crud->check_email_user_exists($email)){
					return true;
					} else {
					return false;
					}
					}

	public function resetPassword()
	{
		$id = $this->uri->segment(4);
		//$pass = $this->input->post('pass');
		$pass =md5("angkasa2021");
		$this->m_crud->updateData($this->table, array('password'=>$pass), $this->pk, $id);
		//echo $this->db->last_query(); exit;
		$this->session->set_flashdata('reset_success','<div class="alert alert-success">Reset Password success.</div>');
		redirect(site_url('admin/petugas'));
	}

	public function cekLogin()
	{
		if ($this->session->userdata('islogin')==false)
			redirect('admin/login','refresh');
	}

	public function gantiPassword()
	{
		$data['title'] = "Ganti Password";
		$this->load->library('form_validation');
		$this->form_validation->set_rules('old', 'Old Password', 'required|min_length[6]');
		$this->form_validation->set_rules('new', 'New Password', 'required|min_length[6]|matches[conf]');
		$this->form_validation->set_rules('conf', 'Password Confirmation', 'required|min_length[6]');
		$this->form_validation->set_error_delimiters('<div class="text-danger" data-animate="fadeInLeft">','</div>');

		if ($this->form_validation->run()==true)
		{
			$this->load->model('m_crud');
			$cek = $this->m_crud->cekPassword('t_user', $this->session->userdata('email'));
			//echo $this->db->last_query(); exit;

			if ($cek->num_rows() > 0)
			{
				$old = md5($this->input->post('old'));
				$pass = $cek->row_array();

				if ($old == $pass['password'])
				{
					$this->db->where( 'id_user', $pass['id_user']);
					$this->db->update('t_user', array('password'=>md5($this->input->post('new'))));
					$this->session->set_flashdata('success', '<div class="alert alert-success">Password berhasil di update</div>');
					redirect('admin/petugas/gantiPassword','refresh');
				}
				else
				{
					$this->session->set_flashdata('gagal', '<div class="alert alert-danger">Password lama anda salah. Coba kembali..</div>');
					redirect('admin/petugas/gantiPassword');
				}

			}
		}

		$this->load->view("admin/petugas/ganti_password", $data);
	}

	
}

/* End of file  */
/* Location: ./application/controllers/ */

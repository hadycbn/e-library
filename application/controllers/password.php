<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Password extends CI_Controller {
  var $table = "t_anggota";
  var $pk    = "id_anggota";

  public function __construct()
	{
		parent::__construct();
    $this->load->library(array('form_validation'));
		$this->load->model('m_crud');
	}
          

			public function index()
			{
				$data['title'] = "Ganti Password";
				$this->load->library('form_validation');
				$this->form_validation->set_rules('old', 'Old Password', 'required');
				$this->form_validation->set_rules('new', 'New Password', 'required|matches[conf]');
				$this->form_validation->set_rules('conf', 'Password Confirmation', 'required');
				$this->form_validation->set_error_delimiters('<div class="text-danger" data-animate="fadeInLeft">','</div>');
		
				if ($this->form_validation->run()==true)
				{
					$this->db->where('username', $this->session->userdata('username'));
					$cek = $this->m_crud->no_paging('t_anggota');
					print_r($this->db->last_query());
					if ($cek->num_rows() > 0)
					{
						$old = md5($this->input->post('old'));
						$pass = $cek->row_array();
		
						if ($old == $pass['password'])
						{
							$this->db->where( 'id_anggota', $pass['id_anggota']);
							$this->db->update('t_anggota', array('password'=>md5($this->input->post('new'))));
							$this->session->set_flashdata('success', '<div class="alert alert-success">Password berhasil di update</div>');
							redirect('password','refresh');
						}
						else
						{
							$this->session->set_flashdata('gagal', '<div class="alert alert-danger">Password lama anda salah. Coba kembali..</div>');
							redirect('password');
						}
		
					}
				}
		
				$this->load->view("ganti_password", $data);
			}
}

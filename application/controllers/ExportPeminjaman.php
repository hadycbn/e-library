<?php
  require 'vendor/autoload.php';
	use PhpOffice\PhpSpreadsheet\Spreadsheet;
	use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

	class ExportPeminjaman extends CI_Controller{

		public function excel()
		{
			$this->load->model('peminjaman_model');
			$spreadsheet = new Spreadsheet();
			$sheet = $spreadsheet->getActiveSheet();
      $sheet->setCellValue('A1', 'No');
			$sheet->setCellValue('B1', 'Kode Peminjaman');
			$sheet->setCellValue('C1', 'Nomor Anggota');
			$sheet->setCellValue('D1', 'Kode Buku');
			$sheet->setCellValue('E1', 'Tanggal Pinjam');
			$sheet->setCellValue('F1', 'Tanggal Kembali');
      $sheet->setCellValue('G1', 'Status Pengembalian');

			$anggota = $this->peminjaman_model->getAll();
			$no = 1;
			$x = 2;
			foreach($anggota as $row)
			{
        if ($row->status == "Y") {
          $status="Sudah di kembalikan";
        }
        elseif ($row->status == "N") {
          $status="Belum di kembalikan";
        }
      	$sheet->setCellValue('A'.$x, $no++);
				$sheet->setCellValue('B'.$x, $row->id_anggota);
				$sheet->setCellValue('C'.$x, $row->nama);
				$sheet->setCellValue('D'.$x, $row->email);
        $sheet->setCellValue('E'.$x, $row->hp);
        $sheet->setCellValue('F'.$x, $row->tgl_daftar);
				$sheet->setCellValue('G'.$x, $status);
				$x++;
			}
      //$tgl=date("Ymd_hms");
      $tgl=date("Ymd");
      $writer = new Xlsx($spreadsheet);
			$filename = 'laporan_peminjaman_'.$tgl;

			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"');
			header('Cache-Control: max-age=0');

			$writer->save('php://output');
		}
	}
?>

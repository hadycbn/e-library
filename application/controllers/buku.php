<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Buku extends CI_Controller {

	var $table = "t_buku";
	var $pk    = "kd_buku";
	var $cat= "status";

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('pagination','form_validation', 'upload'));
		$this->load->model('m_crud');
		$this->cekLogin();
		$this->load->helper(array('url','download'));
	}

	public function index($offset=null)
	{
		$data['title'] = "Data Anggota";
		$aid=$this->session->userdata('username');
		$this->db->where('t_anggota.username', $aid);
		$this->db->where('t_buku.berbayar', 'N');
		$data['buku'] = $this->m_crud->bukusaya($this->db)->result();
		//$data['buku'] = $bukusaya->result();
		$this->load->view("buku/index", $data);

	}


	public function view()
	{
		$data['title'] = "View Buku";
		$id = $this->uri->segment(3);
		$this->cekValidasi();

		//get id
		$get_id = $this->m_crud->get_id($this->table, $this->pk, $id);
		$data['buku'] = $get_id->result();
		$this->load->view("buku/view", $data);
	}

	public function viewberbayar()
	{
		$data['title'] = "View Buku";
		$id = $this->uri->segment(3);
		$this->cekValidasi();

		//get id
		//$get_id = $this->m_crud->get_id($this->table, $this->pk, $id);
		$this->db->where('t_buku.berbayar', 'Y');
		$this->db->where('t_buku.status', 'Y');
		$this->db->where('t_buku.kd_buku', $id);
		$data['buku'] = $this->m_crud->listbuku($this->db)->result();
		//print_r($this->db->last_query());
		//$data['buku'] = $get_id->result();
		$this->load->view("buku/view", $data);
	}

	public function download()
	{
		$data['title'] = "View Buku";
		$id = $this->uri->segment(3);
		$this->cekValidasi();

		//get id
		$get_file = $this->m_crud->get_id($this->table, $this->pk, $id);
		$datafile = $get_file->row_array();
		//$datafile['file_ebook'];

		//get id anggota
		$aid=$this->session->userdata('username');
		$cek = $this->m_crud->get_id('t_anggota', 'username', $aid);
		print_r($this->db->last_query());
		$get = $cek->row_array();
		$tgl=date("Y-m-d");
		

		//insert statistik buku
		$record = array(
			'id_anggota' =>   $get['id_anggota'],
			  'kd_buku' => $id,
			  'tanggal' => $tgl,
			'keterangan' => 'DOWNLOAD'
		 );
		 $this->db->where('id_anggota',  $get['id_anggota']);
		 $this->db->where('kd_buku', $id);
		 $this->db->where('tanggal', $tgl);
		 $statistikbuku = $this->m_crud->no_paging('t_statistik_buku');

		 //Jika statistik sudah ada lakukan update
		 if ($statistikbuku->num_rows() > 0) {
			$idstat = $statistikbuku->row_array();
			$this->m_crud->updateData('t_statistik_buku', $record, 'id', $idstat['id']);	 
		 }else {
			$this->m_crud->insertData('t_statistik_buku', $record);
		 }
		
		$data = file_get_contents("./assets/buku/".$datafile['kategori']."/".$datafile['file_ebook']);
		$nm=$datafile['judul'];
		$name = $nm.'.pdf'; // new name for your file
		//$name = 'myphoto.pdf';
		force_download($name, $data); // start download`

		
		
		$this->load->view("buku/view", $data);
	}

	public function request()
	{
		$data['title'] = "Request Akses";
		$id = $this->uri->segment(3);
		$tgl=date("Y-m-d");
		$nextmonth = date('Y-m-d', strtotime('+1 month'));

		//get id buku
		$get_id = $this->m_crud->get_id($this->table, $this->pk, $id);

		//get id anggota
		$aid=$this->session->userdata('username');
		$cek = $this->m_crud->get_id('t_anggota', 'username', $aid);
		$get = $cek->row_array();


		$this->editValidasi();
		if ($this->form_validation->run()==true)
		{
			$record = array(
				'id_anggota' =>   $get['id_anggota'],
				'kd_buku' => $id,
				'tgl_submit' => $tgl,
				'tgl_expired' => $nextmonth,
				'status' => 'N'
		   );
			

							   $this->m_crud->insertData('t_buku_berbayar', $record);
							   $this->session->set_flashdata('add_success', '<div class="alert alert-success"> Permintaan Akses Buku berbayar berhasil, staff admin kami akan verifikasi permintaan anda.</div>');
							   redirect('akses/index');
		}

		$data['buku'] = $get_id->result();
		$this->load->view("buku/request", $data);
	}

	public function editValidasi()
	{
		$this->form_validation->set_rules('kd_buku', 'Kode Buku', 'required');
		$this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');
	}


	public function baca()
	{
		$data['title'] = "View Buku";
		$id = $this->uri->segment(3);
		$this->cekValidasi();

		//get id buku
		$data['buku'] = $this->m_crud->get_id($this->table, $this->pk, $id)->result();
		
		//get id anggota
		$aid=$this->session->userdata('username');
		$cek = $this->m_crud->get_id('t_anggota', 'username', $aid);
		$get = $cek->row_array();
		$tgl=date("Y-m-d");
		

		//insert statistik buku
		$record = array(
			'id_anggota' =>   $get['id_anggota'],
			  'kd_buku' => $id,
			  'tanggal' => $tgl,
			'keterangan' => 'BACA'
		 );
		 $this->db->where('id_anggota',  $get['id_anggota']);
		 $this->db->where('kd_buku', $id);
		 $this->db->where('tanggal', $tgl);
		 $statistikbuku = $this->m_crud->no_paging('t_statistik_buku');

		 //Jika statistik sudah ada lakukan update
		 if ($statistikbuku->num_rows() > 0) {
			$idstat = $statistikbuku->row_array();
			$this->m_crud->updateData('t_statistik_buku', $record, 'id', $idstat['id']);	 
		 }else {
			$this->m_crud->insertData('t_statistik_buku', $record);
		 }

		//print_r($this->db->last_query());
		$this->load->view("buku/baca", $data);
	}
	public function kategori($offset=null)
	{
		$data['title'] = "";
		$cat = $this->uri->segment(3);
		$limit = 10;

		$this->db->where('t_kategori.kode', $cat);
		$data['katalog'] = $this->m_crud->listbuku($this->db)->result();

		$this->db->where('kode', $cat);
		$data['kategori'] = $this->m_crud->kategori();
		//print_r($this->db->last_query());
		$this->load->view("buku/kategori", $data);
	}

	
	public function all()
	{
		$data['title'] = "Data Buku";
		//$limit = 10000;
		$this->db->where('t_buku.berbayar', 'N');
		$this->db->where('t_buku.status', 'Y');
		$this->db->order_by('kd_buku', 'desc');
		$data['allbook'] = $this->m_crud->listbuku($this->db)->result();
		//print_r($this->db->last_query());
		$this->load->view("buku/semuabuku", $data);

	}

	public function berbayar()
	{
		$data['title'] = "Data Buku Berbayar";
		//get id anggota
		$aid=$this->session->userdata('username');
		$cek = $this->m_crud->get_id('t_anggota', 'username', $aid);
		$get = $cek->row_array();

		#Create where clause
		$kdbuku = $this->m_crud->get_id('t_buku_berbayar', 'id_anggota', $get['id_anggota']);
		$query1_result = $kdbuku->result();
		$kd_buku= array();
		foreach($query1_result as $row){
			$kd_buku[] = $row->kd_buku;
		}
		$book = implode(",",$kd_buku);
		$ids = explode(",", $book);
		//$getkdbuku = $kdbuku->row_array();
		//$kd=$getkdbuku['kd_buku'];

		#Create main query
		$this->db->where('t_buku.berbayar', 'Y');
		$this->db->where('t_buku.status', 'Y');
		//$this->db->where('t_buku.kd_buku NOT IN', array('kd_buku' => $row->kd_buku));
		$this->db->where_not_in('t_buku.kd_buku', $ids);
		$data['berbayar'] = $this->m_crud->listbuku($this->db)->result();
		//print_r($this->db->last_query());
		$this->load->view("buku/berbayar", $data);

	}


	

	public function cekValidasi()
	{
		//$this->form_validation->set_rules('judul', 'Judul', 'required');
		//$this->form_validation->set_rules('kategori', 'Kategori', 'required');
		//$this->form_validation->set_rules('penerbit', 'Penerbit', 'required');
		//$this->form_validation->set_rules('pengarang', 'Pengarang', 'required');
		//$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
		//$this->form_validation->set_rules('foto', 'File E-book', 'required');
		//$this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');
	}

	public function cekLogin()
	{
		if ($this->session->userdata('islogin')==false)
			redirect('login','refresh');
	}



}

/* End of file  */
/* Location: ./application/controllers/ */

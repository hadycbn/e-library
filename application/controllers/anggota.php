<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anggota extends CI_Controller {

	var $table = "t_anggota";
	var $pk = "id_anggota";

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation', 'upload', 'pagination'));
		$this->load->model('m_crud');
		$this->cekLogin();
	}

	public function index()
	{
		$data['title'] = "Edit Anggota";
		$aid=$this->session->userdata('username');
		$query = $this->m_crud->get_id($this->table, 'username', $aid);
		

		$this->cekValidasi();
		if ($this->form_validation->run()==true)
		{
			//upload
			$config['upload_path'] = './assets/img/anggota/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '';
			$config['max_width']  = '';
			$config['max_height']  = '';
			$id=$this->input->post('id_anggota');

			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('foto')){
				$record = array(
								 'nama' => $this->input->post('nama'),
								 'email' => $this->input->post('email'),
								 'hp' => $this->input->post('phone')
								 
					       	   );
			}
			else{
				$foto = $_FILES['foto']['name'];
				$record = array(
								'nama' => $this->input->post('nama'),
								'email' => $this->input->post('email'),
								'hp' => $this->input->post('phone'),
								'foto' => $foto								
								);

				$unlink_id = $query->row_array();
				unlink('./assets/img/anggota/'.$unlink_id['foto']);
			}

			$this->m_crud->updateData($this->table, $record, $this->pk, $id);
			//print_r($this->db->last_query());
			$this->session->set_flashdata('success','<div class="alert alert-success"> Update profil anggota berhasil</div>');
			redirect('anggota/index');
		}

		$data['anggota'] = $query->result();
		$this->load->view("anggota/index", $data);

	}

	public function tambah()
	{
		$data['title'] = "Tambah Anggota";
		$this->cekValidasi();

		if ($this->form_validation->run()==true)
		{
			//upload
			$config['upload_path'] = './assets/img/anggota/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']  = '5000';
				$config['max_width']  = '3126';
				$config['max_height']  = '4224';

				$this->upload->initialize($config);
				if ( ! $this->upload->do_upload('foto')){
					$foto = "";
				}
				else{
					$foto = $_FILES['foto']['name'];
				}

				$record = array(
	        'id_anggota' => $this->input->post('id_anggota'),
	        'nama' => $this->input->post('nama'),
	        'email' => $this->input->post('email'),
	        'hp' => $this->input->post('phone'),
	        'username' => $this->input->post('username'),
	        'password' => md5($this->input->post('password')),
					'foto' => $foto,
	        'tgl_daftar' => date("Y-m-d"),
	        'status' => 'InActive'
							   );

				$this->m_crud->insertData($this->table, $record);
				$this->session->set_flashdata('add_success', '<div class="alert alert-success">Data berhasil di input</div>');
				redirect('anggota','refresh');
		}

		$data['autonumber'] = $this->m_crud->autoNumber($this->table, $this->pk, 3, date("Ymd"));
		$this->template->display('anggota/tambah', $data);
	}

	

	public function hapus()
	{
		$id = $this->input->post('id_hapus');
		$detail = $this->m_crud->get_id($this->table, $this->pk, $id)->row_array();
		unlink("assets/img/anggota/".$detail['foto']);

		$this->m_crud->deleteData($this->table, $this->pk, $id);
		$this->session->set_flashdata('delete_success', '<div class="alert alert-danger">Data berhasil dihapus</div>');
	}

	public function cekValidasi()
	{
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('phone', 'phone', 'required|numeric');
		$this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');
	}

			// Check if username exists
		public function check_username_exists($username){
			$this->form_validation->set_message('check_username_exists', 'Username already exists, please revise');
		 if($this->m_crud->check_username_exists($username)){
		 return true;
		 } else {
		 return false;
		 }
		}
				// Check if username exists
		public function check_phone_exists($phone){
		$this->form_validation->set_message('check_phone_exists', 'Phone number already exists, please revise');
		if($this->m_crud->check_phone_exists($phone)){
		return true;
		} else {
		return false;
		}
		}
		// Check if username exists
		public function check_email_exists($email){
		$this->form_validation->set_message('check_email_exists', 'Email already exists, please revise');
		if($this->m_crud->check_email_exists($email)){
		return true;
		} else {
		return false;
		}
		}

	public function cariData()
	{
		$data['title'] = "Hasil Pencarian Data Anggota";
		//set value
		$input = $this->input->post('cari');
		$this->db->like('nim', $input);
		$this->db->or_like('nama', $input);
		$query = $this->db->get($this->table);
		//echo $this->db->last_query(); exit;
			$cek_data = $query->num_rows();

		if ($cek_data > 0)
		{
			$data['anggota'] = $query->result();
			$this->template->display('anggota/cari', $data);
			$this->session->set_flashdata('filter_success', '<div class="alert alert-success">Pencarian Sukses</div>');
		}
		else {
			$data['message'] = '<div class="alert alert-danger">OOPs ... Data yang anda Cari Tidak ada</div>';
			$this->template->display('anggota/tidakada', $data);
		}

	}

	public function cekLogin()
	{
		if ($this->session->userdata('islogin') == false)
			redirect('login','refresh');
	}
}

/* End of file anggota.php */
/* Location: ./application/controllers/anggota.php */

<?php
  require 'vendor/autoload.php';
	use PhpOffice\PhpSpreadsheet\Spreadsheet;
	use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

	class ExportPengembalian extends CI_Controller{

		public function excel()
		{
			$this->load->model('pengembalian_model');
			$spreadsheet = new Spreadsheet();
			$sheet = $spreadsheet->getActiveSheet();
      $sheet->setCellValue('A1', 'No');
			$sheet->setCellValue('B1', 'Kode Peminjaman');
			$sheet->setCellValue('C1', 'Kode Buku');
			$sheet->setCellValue('D1', 'Tanggal Kembali');
      $sheet->setCellValue('E1', 'Denda');
      $sheet->setCellValue('F1', 'Petugas');

			$siswa = $this->pengembalian_model->getAll();
			$no = 1;
			$x = 2;
			foreach($siswa as $row)
			{

      	$sheet->setCellValue('A'.$x, $no++);
				$sheet->setCellValue('B'.$x, $row->kd_peminjaman);
				$sheet->setCellValue('C'.$x, $row->kd_buku);
        $sheet->setCellValue('D'.$x, $row->tgl_kembali);
        $sheet->setCellValue('E'.$x, $row->denda);
				$sheet->setCellValue('F'.$x, $row->petugas);
				$x++;
			}
      //$tgl=date("Ymdhms");
      $tgl=date("Ymd");
      $writer = new Xlsx($spreadsheet);
			$filename = 'laporan_pengembalian_'.$tgl;

			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"');
			header('Cache-Control: max-age=0');

			$writer->save('php://output');
		}
	}
?>

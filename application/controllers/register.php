<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends CI_Controller {
  var $table = "t_anggota";
  var $verifikasi = "t_anggota_verification";
  var $pk    = "id_anggota";

  public function __construct()
	{
		parent::__construct();
		$this->load->library(array('pagination','form_validation', 'upload'));
		$this->load->model('m_crud');
	}

  public function index()
  {
    $data['title'] = "";
    //set rule validate
    $this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_check_email_exists');
		$this->form_validation->set_rules('phone', 'phone', 'required|numeric|callback_check_phone_exists');
    $this->form_validation->set_rules('username', 'Username','required|callback_check_username_exists');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
		$this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');

    if ($this->form_validation->run()==true)
		{
			$record = array(
        'id_anggota' => $this->input->post('id_anggota'),
        'nama' => $this->input->post('nama'),
        'email' => $this->input->post('email'),
        'hp' => $this->input->post('phone'),
        'username' => $this->input->post('username'),
        'password' => md5($this->input->post('password')),
        'tgl_daftar' => date("Y-m-d"),
        'status' => 'N'
        //'foto' => $foto
               );
               
      $verifikasi = array(
        'id_anggota' => $this->input->post('id_anggota'),
        'kode' => md5($this->input->post('email')),
        'tgl_input' => date("Y-m-d")
        //'foto' => $foto
						   );

      $this->m_crud->insertData($this->table, $record);
      $this->m_crud->insertData($this->verifikasi, $verifikasi);
  			//$this->session->set_flashdata('add_success', '<div class="alert alert-success">Data sukses ditambahkan</div>');
  			redirect('register/konfirmasi','refresh');
  		}
      $data['autonumber'] = $this->m_crud->autoNumber($this->table, $this->pk, 3, date("Ymd"));
  		//$this->template->display('anggota/tambah', $data);
      //$this->template->display('register', $data);
      $this->load->view("register", $data);
      }

      public function konfirmasi()
      {
        $data['title'] = "";
        $this->template->display('konfirmasi', $data);
      }

      public function forgotpassword()
      {
        $data['title'] = "";
        $this->template->display('forgotpassword', $data);
      }
      // Check if username exists
    public function check_username_exists($username){
      $this->form_validation->set_message('check_username_exists', 'Username already exists, please revise');
     if($this->m_crud->check_username_exists($username)){
     return true;
     } else {
     return false;
     }
    }
        // Check if username exists
    public function check_phone_exists($phone){
    $this->form_validation->set_message('check_phone_exists', 'Phone number already exists, please revise');
    if($this->m_crud->check_phone_exists($phone)){
    return true;
    } else {
    return false;
    }
    }
    // Check if username exists
    public function check_email_exists($email){
    $this->form_validation->set_message('check_email_exists', 'Email already exists, please revise');
    if($this->m_crud->check_email_exists($email)){
    return true;
    } else {
    return false;
    }
    }
}

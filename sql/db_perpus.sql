-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 12, 2021 at 12:28 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.2.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_perpus`
--

-- --------------------------------------------------------

--
-- Table structure for table `temp`
--

CREATE TABLE `temp` (
  `kd_buku` varchar(15) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `pengarang` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_anggota`
--

CREATE TABLE `t_anggota` (
  `id_anggota` varchar(20) NOT NULL,
  `nama` varchar(70) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `hp` varchar(20) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `tgl_daftar` date NOT NULL,
  `status` enum('Y','N') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_anggota`
--

INSERT INTO `t_anggota` (`id_anggota`, `nama`, `email`, `hp`, `username`, `password`, `foto`, `tgl_daftar`, `status`) VALUES
('20210628048', 'suha', 'suhadi@yahoo.com', '08129', 'hadycbn', '46f94c8de14fb36680850768ff1b7f2a', NULL, '2021-06-28', 'Y'),
('20210628050', 'Hayfa', 'hayfa@nuzhatul.fikrah', '0817', 'hayfa', '46f94c8de14fb36680850768ff1b7f2a', NULL, '2021-06-28', 'Y'),
('20210628051', 'Koswara', 'engkos@koswara.com', '08119090', 'kucrut', '46f94c8de14fb36680850768ff1b7f2a', NULL, '2021-06-28', 'Y'),
('20210628052', 'Kang Mus', 'kang@mus.com', '081290', 'hayfanuz', 'efe6398127928f1b2e9ef3207fb82663', NULL, '2021-06-28', 'Y'),
('20210629053', 'Nuzhatul', 'nuzhatul@yahoo.com', '0817123', 'nuzhatul', '46f94c8de14fb36680850768ff1b7f2a', NULL, '2021-06-29', 'Y'),
('20210629054', 'Kang Murad', 'murad@kangmus.com', '0899', 'murad', 'a8f5f167f44f4964e6c998dee827110c', 'photo-removebg-preview.png', '2021-06-29', 'Y'),
('20210706057', 'Agus', 'agus12@firman.com', '081122712', 'suha@yahoo.com', '46f94c8de14fb36680850768ff1b7f2a', '58334.jpg', '2021-07-06', 'Y'),
('20210706058', 'Alif M', 'alifmagh12@gmail.com', '089612', 'alifmagh@gmail.com', 'e120ea280aa50693d5568d0071456460', 'banner_plts.jpg', '2021-07-06', 'Y'),
('20210708059', 'Agus Wiranto', 'wiranto@agus.com', '0812911', 'wiranto@agus.com', '96f0f08c0188ba04898ce8cc465c19c4', 'ping.png', '2021-07-08', ''),
('20210708060', 'Walidi', 'walidi@gmail.com', '0877', 'walidi@gmail.com', '46f94c8de14fb36680850768ff1b7f2a', 'default-logo.png', '2021-07-08', ''),
('20210708061', 'Zulham', 'zulham@zamrud.com', '0878', 'zulham', '46f94c8de14fb36680850768ff1b7f2a', '58334.jpg', '2021-07-08', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `t_anggota_verification`
--

CREATE TABLE `t_anggota_verification` (
  `id_anggota` varchar(20) NOT NULL,
  `kode` varchar(100) NOT NULL,
  `tgl_input` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_anggota_verification`
--

INSERT INTO `t_anggota_verification` (`id_anggota`, `kode`, `tgl_input`) VALUES
('20210628048', '12123', '2021-06-29'),
('20210628052', '', '2021-06-29'),
('20210628048', '615fdf364d4e35072a62861941aa4189', '2021-06-29'),
('20210628050', '5b502aaff83917a270059e8815fab937', '2021-06-29'),
('20210628051', 'e198f58c12c576e54784d94b8b5db911', '2021-06-29'),
('20210628051', 'e198f58c12c576e54784d94b8b5db911', '2021-06-29'),
('20210628051', 'e198f58c12c576e54784d94b8b5db911', '2021-06-29'),
('20210628048', '615fdf364d4e35072a62861941aa4189', '2021-06-29'),
('20210629054', '005bd1506223173f0e5f03729037bfde', '2021-06-29'),
('20210703056', '46f94c8de14fb36680850768ff1b7f2a', '2021-07-03'),
('20210708061', 'd71559bc7c24028e170305c3a41b4d38', '2021-07-08');

-- --------------------------------------------------------

--
-- Table structure for table `t_buku`
--

CREATE TABLE `t_buku` (
  `kd_buku` varchar(10) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `penerbit` varchar(50) NOT NULL,
  `pengarang` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `kategori` int(11) NOT NULL,
  `cover` varchar(100) NOT NULL,
  `file_ebook` varchar(100) NOT NULL,
  `tgl_masuk` date NOT NULL,
  `statistik` int(11) DEFAULT NULL,
  `berbayar` enum('Y','N') DEFAULT NULL,
  `status` enum('Y','N') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_buku`
--

INSERT INTO `t_buku` (`kd_buku`, `judul`, `penerbit`, `pengarang`, `deskripsi`, `kategori`, `cover`, `file_ebook`, `tgl_masuk`, `statistik`, `berbayar`, `status`) VALUES
('BUK34', 'Dongeng Anak Remaja', 'Edy', 'Sutarzan', 'Test add buku                                            ', 1, '6b7c3f99a0cfaa352c3f08d487dca62c.jpg', '647df662eea7e9ffe3845d55462c4a18.pdf', '2021-07-03', NULL, 'N', 'Y'),
('BUK35', 'Kancil dan Kucing', 'Sahabat', 'Umi kulstum', '                                                                Test                                            ', 2, 'eb2572e8b06b1249ec23a2ca2aaafd2c.jpg', '496c545e93d6f4e23417572bfaa595cb.pdf', '2021-07-03', NULL, 'Y', 'N'),
('BUK38', '100 resep masakan jogja', 'chef rudi', 'chef juna', '<p>&nbsp;Buku ini memiliki 100 resep makanan yang enak</p>', 3, '', '', '2021-07-06', NULL, 'N', 'Y'),
('BUK41', 'Novel Tere', 'Liye', 'Tere Liye', 'dadasd                 ', 4, '', 'd253b45150f95d66d8a6b86fae2138d3.pdf', '2021-07-03', NULL, 'N', 'Y'),
('BUK42', 'Buku Biografi', 'Alyn', 'Zalfa', 'Buku Biografi anak SMP                                            ', 5, '', 'a78ca0e6a452925741c0c0afea433d5a.pdf', '2021-07-03', NULL, 'N', 'Y'),
('BUK43', 'Dongeng Anak', 'Koko', 'Kiki', 'TEst                                            ', 6, '947aa2b07e91fd3cf80e61daceb497be.jpg', '9fcddaa83cab84b9b55de8352104be51.pdf', '2021-07-06', NULL, 'N', 'Y'),
('BUK44', 'Dongeng Anak Remaja', 'Edy', 'Sutarzan', 'Test add buku                                            ', 7, '6b7c3f99a0cfaa352c3f08d487dca62c.jpg', '647df662eea7e9ffe3845d55462c4a18.pdf', '2021-07-03', NULL, 'N', 'Y'),
('BUK45', 'Kancil dan Kucing', 'Sahabat', 'Umi kulstum', '                                                                Test                                            ', 9, 'eb2572e8b06b1249ec23a2ca2aaafd2c.jpg', '496c545e93d6f4e23417572bfaa595cb.pdf', '2021-07-03', NULL, 'Y', 'N'),
('BUK48', '100 resep masakan jogja', 'chef rudi', 'chef juna', '<p>&nbsp;Buku ini memiliki 100 resep makanan yang enak</p>', 11, '3588f4534ca13bc697d97207adbace67.PNG', '1d8f9f05f558725958c0d76bd795e83e.pdf', '2021-07-04', NULL, 'Y', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `t_buku_berbayar`
--

CREATE TABLE `t_buku_berbayar` (
  `id` int(11) NOT NULL,
  `id_anggota` varchar(20) NOT NULL,
  `kd_buku` varchar(10) NOT NULL,
  `tgl_submit` date NOT NULL,
  `tgl_expired` date NOT NULL,
  `status` enum('Y','N') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_buku_berbayar`
--

INSERT INTO `t_buku_berbayar` (`id`, `id_anggota`, `kd_buku`, `tgl_submit`, `tgl_expired`, `status`) VALUES
(1, '20210629053', 'BUK34', '2021-07-10', '2022-07-31', 'N'),
(2, '20210628052 ', 'BUK35', '2021-07-10', '2021-07-20', 'Y'),
(3, '20210628048', 'BUK38', '2021-07-10', '2021-07-01', 'Y'),
(4, '20210629049', 'BUK41', '2021-07-10', '2021-06-30', 'N'),
(5, '20210628050', 'BUK42', '2021-07-10', '2021-08-01', 'Y'),
(6, '20210629051', 'BUK43', '2021-07-10', '2021-09-01', 'N'),
(7, '20210628052', 'BUK44', '2021-07-10', '2021-10-01', 'N'),
(8, '20210629053', 'BUK45', '2021-07-10', '2021-11-01', 'N'),
(10, '20210628048', 'BUK22', '2021-07-10', '2021-06-29', 'N'),
(11, '20210629049', 'BUK23', '2021-07-10', '2021-05-05', 'N'),
(12, '20210628050', 'BUK24', '2021-07-10', '2021-07-01', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `t_kategori`
--

CREATE TABLE `t_kategori` (
  `kode` int(11) NOT NULL,
  `kategori` varchar(50) NOT NULL,
  `template` varchar(50) NOT NULL,
  `status` enum('Y','N') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_kategori`
--

INSERT INTO `t_kategori` (`kode`, `kategori`, `template`, `status`) VALUES
(1, 'KOMIK', 'card bg-primary text-white shadow', 'Y'),
(2, 'NOVEL', 'card bg-success text-white shadow', 'Y'),
(4, 'BIOGRAFI', 'card bg-warning text-white shadow', 'Y'),
(5, 'DONGENG', 'card bg-danger text-white shadow', 'Y'),
(6, 'ILMU PENGETAHUAN', 'card bg-secondary text-white shadow', 'Y'),
(7, 'CERITA BERGAMBAR', 'card bg-light text-black shadow', 'Y'),
(8, 'Cerita Anak', 'card bg-dark text-white shadow', 'Y'),
(9, 'Sains dan Teknologi', 'card bg-primary text-white shadow', 'N'),
(11, 'Keterampilan Memasak', 'card bg-info text-white shadow', 'Y'),
(13, 'Sosiologi', 'card bg-danger text-white shadow', 'Y'),
(14, 'Sosiologi', 'card bg-secondary text-white shadow', 'Y'),
(15, 'Biologi', 'ard bg-light text-black shadow', 'Y'),
(17, 'Luar Angkasa', 'card bg-warning text-white shadow', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `t_peminjaman`
--

CREATE TABLE `t_peminjaman` (
  `kd_peminjaman` varchar(15) NOT NULL,
  `id_anggota` varchar(15) NOT NULL,
  `kd_buku` varchar(10) NOT NULL,
  `tgl_pinjam` date NOT NULL,
  `tgl_kembali` date NOT NULL,
  `status` enum('y','n') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_peminjaman`
--

INSERT INTO `t_peminjaman` (`kd_peminjaman`, `id_anggota`, `kd_buku`, `tgl_pinjam`, `tgl_kembali`, `status`) VALUES
('20210615004', '20151202003', 'BUK02', '2021-06-15', '2021-06-22', 'n'),
('20210615005', '20151202003', 'BUK02', '2021-06-15', '2021-06-22', 'n'),
('20210615006', '20151202003', 'BUK02', '2021-06-15', '2021-06-22', 'n'),
('20210615007', '20151201001', 'BUK03', '2021-06-15', '2021-06-22', 'y'),
('20210615007', '20151201001', 'BUK01', '2021-06-15', '2021-06-22', 'y'),
('20210615007', '20151201001', 'BUK02', '2021-06-15', '2021-06-22', 'n'),
('20210615008', '20151201002', 'BUK04', '2021-06-15', '2021-06-22', 'n'),
('20210615008', '20151201002', 'BUK05', '2021-06-15', '2021-06-22', 'n'),
('20210616009', '20151201002', 'BUK03', '2021-06-16', '2021-06-23', 'n'),
('20210617010', '20151201002', 'BUK04', '2021-06-17', '2021-06-24', 'n');

-- --------------------------------------------------------

--
-- Table structure for table `t_pengembalian`
--

CREATE TABLE `t_pengembalian` (
  `kd_peminjaman` varchar(15) NOT NULL,
  `kd_buku` varchar(15) NOT NULL,
  `tgl_kembali` date NOT NULL,
  `denda` int(11) NOT NULL,
  `petugas` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_pengembalian`
--

INSERT INTO `t_pengembalian` (`kd_peminjaman`, `kd_buku`, `tgl_kembali`, `denda`, `petugas`) VALUES
('20160126001', 'BUK01', '2016-01-29', 0, 'pandu'),
('20160126001', 'BUK02', '2016-01-30', 2000, 'pandu'),
('20210615007', 'BUK03', '2021-06-15', 0, 'pandu'),
('20210615007', 'BUK01', '2021-06-15', 0, 'pandu');

-- --------------------------------------------------------

--
-- Table structure for table `t_statistik_buku`
--

CREATE TABLE `t_statistik_buku` (
  `id` int(11) NOT NULL,
  `id_anggota` varchar(20) NOT NULL,
  `kd_buku` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_statistik_buku`
--

INSERT INTO `t_statistik_buku` (`id`, `id_anggota`, `kd_buku`, `tanggal`, `keterangan`) VALUES
(1, '20210628052', 'BUK34', '2021-07-01', 'BACA'),
(2, '20210629053', 'BUK34', '2021-07-01', 'DOWNLOAD'),
(3, '20210628048', 'BUK35', '2021-07-01', 'BACA'),
(4, '20210629049', 'BUK38', '2021-06-01', 'DOWNLOAD'),
(5, '20210628050', 'BUK41', '2021-05-01', 'BACA'),
(6, '20210629051', 'BUK42', '2021-04-01', 'DOWNLOAD'),
(7, '20210628052', 'BUK43', '2021-03-01', 'BACA'),
(8, '20210629053', 'BUK44', '2021-02-01', 'DOWNLOAD'),
(9, '20210629054', 'BUK45', '2021-01-01', 'BACA'),
(10, '20210628048', 'BUK48', '2021-07-01', 'DOWNLOAD'),
(11, '20210628048', 'BUK48', '2021-06-01', 'BACA'),
(12, '20210628048', 'BUK48', '2021-05-01', 'DOWNLOAD');

-- --------------------------------------------------------

--
-- Table structure for table `t_statistik_pengunjung`
--

CREATE TABLE `t_statistik_pengunjung` (
  `id` int(11) NOT NULL,
  `id_anggota` varchar(20) NOT NULL,
  `tanggal` date NOT NULL,
  `ip` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_statistik_pengunjung`
--

INSERT INTO `t_statistik_pengunjung` (`id`, `id_anggota`, `tanggal`, `ip`) VALUES
(1, '20210628052', '2021-06-15', '202.176.211.105'),
(2, '20210629053', '2021-07-01', '192.168.8.101'),
(3, '20210628048', '2021-07-01', '202.176.211.105'),
(4, '20210629049', '2021-06-01', '192.168.8.101'),
(5, '20210628050', '2021-05-01', '202.176.211.105'),
(6, '20210629051', '2021-04-01', '192.168.8.101'),
(7, '20210628052', '2021-03-01', '202.176.211.105'),
(8, '20210629053', '2021-02-01', '192.168.8.101'),
(9, '20210629054', '2021-01-01', '202.176.211.105'),
(10, '20210628048', '2021-07-01', '192.168.8.101'),
(11, '20210629049', '2021-06-01', '202.176.211.105'),
(12, '20210628050', '2021-05-01', '192.168.8.101');

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE `t_user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(70) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `hp` varchar(20) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` enum('1','2') NOT NULL,
  `status` enum('Y','N') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`id_user`, `nama`, `email`, `hp`, `username`, `password`, `level`, `status`) VALUES
(1, 'pandu siwi', 'pandu@winata.com', '08127778317', 'pandu', 'a8f5f167f44f4964e6c998dee827110c', '1', 'Y'),
(2, 'Suhadi', 'hadycbn@yahoo.com', '1111', 'suha', '512a694f680daef8f5128ba206863da0', '2', 'Y'),
(4, 'petugas1', 'petu@gas.com', '811297', 'petugas1', '4f8e22eeae6e3b21cdd0b8fb4a0557fd', '2', 'Y'),
(6, 'Zalfa', 'zalfa@ymail.net', '8129', 'zalfaon', 'd41d8cd98f00b204e9800998ecf8427e', '2', 'Y'),
(7, 'alyn', 'alyn@gmail.com', '08129', 'alyn', '4f8e22eeae6e3b21cdd0b8fb4a0557fd', '2', 'Y'),
(8, 'hayfa', 'has@ewas.com', '08129', 'admin', '0', '2', 'Y'),
(12, 'hayfa', 'hansamu@gas.com', '0726112', 'adminhayfa', '4f8e22eeae6e3b21cdd0b8fb4a0557fd', '2', 'Y');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_anggota`
--
ALTER TABLE `t_anggota`
  ADD PRIMARY KEY (`id_anggota`);

--
-- Indexes for table `t_buku`
--
ALTER TABLE `t_buku`
  ADD PRIMARY KEY (`kd_buku`);

--
-- Indexes for table `t_buku_berbayar`
--
ALTER TABLE `t_buku_berbayar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kategori`
--
ALTER TABLE `t_kategori`
  ADD PRIMARY KEY (`kode`);

--
-- Indexes for table `t_statistik_buku`
--
ALTER TABLE `t_statistik_buku`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_statistik_pengunjung`
--
ALTER TABLE `t_statistik_pengunjung`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_buku_berbayar`
--
ALTER TABLE `t_buku_berbayar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `t_kategori`
--
ALTER TABLE `t_kategori`
  MODIFY `kode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `t_statistik_buku`
--
ALTER TABLE `t_statistik_buku`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `t_statistik_pengunjung`
--
ALTER TABLE `t_statistik_pengunjung`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `t_user`
--
ALTER TABLE `t_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
